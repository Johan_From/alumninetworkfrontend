import { useUser } from "../context/UserContext"
import { Navigate } from 'react-router-dom'

// Method for checking if you are logged in or not
const withAuth = Component => props => {
    const { user } = useUser();
    if(user !== null){
        return <Component {...props} />
    } else{
        return <Navigate to="/"/>
    }
}
export default withAuth;

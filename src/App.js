import './App.css';
import { BrowserRouter as Router, Routes, Route} from 'react-router-dom'
import Timeline from './views/Timeline/Timeline';
import PageNotFound from './views/PageNotFound/PageNotFound';
import UserProfile from './views/UserProfile/UserProfile';
import PublicProfiles from './views/PublicProfiles/PublicProfiles';
import HomePage from './views/HomePage/HomePage'
import Groups from './views/Groups/Groups';
import Events from './views/Events/Events';
import DirectMessage from './views/DirectMessage/DirectMessage';
import Topics from './views/Topics/Topics';
import Navbar from './components/Navbar/Navbar';
import Footer from './components/Footer/Footer';
import MessageButton from './components/Buttons/MessageButton';
import SpecificGroup from './components/Groups/SpecificGroup';
import About from './views/About/About';
import AddGroup from './views/AddGroup/AddGroup';

function App() {
  return (
    <Router>
      <div className="App">
        <Navbar/>
        <Routes>
        <Route path="/" element={<HomePage/> }/>
          <Route path="/timeline" element={<Timeline/> }/>
          <Route path='/userprofile' element={<UserProfile/> }/>
          <Route path='/profiles' element={<PublicProfiles/> }/>
          <Route path='/groups' element={<Groups/> }/>
          <Route path='/groups/:id' element={<SpecificGroup/>}/>
          <Route path='/events' element={<Events/> }/>
          <Route path='/topics' element={<Topics/> }/>
          <Route path='/directmessage' element={<DirectMessage/> }/>
          <Route path="/about" element={<About/>}/>
          <Route path="/addgroup" element={<AddGroup/>} />

          <Route path="*" element={ <PageNotFound/> }/>
        </Routes>
        <MessageButton/>
        <Footer/>
      </div>
    </Router>
  );
}

export default App;

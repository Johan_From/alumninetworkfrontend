import Keycloak from "keycloak-js";

// Init keycloak
const keycloak = new Keycloak('keycloak.json')

export const initialize = () => {
    const config = {
      checkLoginIframe: false,
      onLoad: "check-sso",
    };
    return keycloak.init(config);
};

export default keycloak;
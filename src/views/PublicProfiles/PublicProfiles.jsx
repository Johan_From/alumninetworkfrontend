import { Box, TextField, Typography, Pagination } from "@mui/material";
import { useEffect, useState } from "react";
import { getAllUsers } from "../../api/userEndpoints";
import SpecificUser from "../../components/SpecificUser/SpecificUser";
import withAuth from "../../hoc/withAuth";
import usePagination from "../../utils/Pagination";

const PublicProfiles = () => {
    const [users, setUsers] = useState([]);
    const [searchName, setSearchName] = useState("")
    let [usersPage, setUsersPage] = useState(1)
    
    // Pagination variables
    const USERS_PER_PAGE = 20;
    const users_count = Math.ceil(users.length / USERS_PER_PAGE)

    const USERS_DATA = usePagination(users, USERS_PER_PAGE)
    
    
    // Fetching all the users
    useEffect(() => {
        let abortController = new AbortController();
        const fetchData = async () => {
            const response = await getAllUsers();
            setUsers(response.data)
        }

        fetchData();

        return () => {  
            abortController.abort();  
        }  
    }, [])

    // Handle pagination
    const handleUsersChange = (e, p) => {
        setUsersPage(p)
        USERS_DATA.jump(p)
    }

    return(
        <div>
            {/* All the profiles */}
            <Typography variant="h4" sx={{ marginTop: 3, marginBottom: 2}}>Public profiles </Typography>
            <TextField label="Search for user" sx={{marginBottom: 3, width: 300}} type="text" autoComplete="off" onChange={(e) => setSearchName(e.target.value)}/>
            <Box sx={{display: 'flex', flexWrap: 'wrap', justifyContent: 'center'}}>
                {USERS_DATA.currentData().filter((u) => {
                    if(searchName === ""){
                        return u
                    }
                    else if(u.name.toLowerCase().includes(searchName.toLowerCase())){
                        return u
                    }
                    return null
                }).map((u, i) => (
                    <>
                     {/* Component for the specific user */}
                        <SpecificUser users={u} key={i}/> 
                    </>
                ))}
                
            </Box>
            <div >
                <Pagination
                count={users_count}
                size="large"
                page={usersPage}
                variant="outlined"
                shape="rounded"
                onChange={handleUsersChange}
                style={{
                display: "flex", 
                flexDirection: "row",
                justifyContent: "center",
                }}
                sx={{marginBottom: 2}}
                />
            </div>
        </div>
    )
}

export default withAuth(PublicProfiles);
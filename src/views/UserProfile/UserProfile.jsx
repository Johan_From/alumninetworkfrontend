import { useUser } from "../../context/UserContext";
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import { forwardRef, useEffect, useState } from "react";
import { Accordion, AccordionDetails, AccordionSummary, Button, Card, Snackbar, Tooltip, Typography } from "@mui/material";
import { getSpecificUser, patchUser } from "../../api/userEndpoints";
import MuiAlert from '@mui/material/Alert';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import UpdateIcon from '@mui/icons-material/Update';
import { getDirectPostToUser } from "../../api/postEndpoints";
import moment from 'moment'
import { styled } from '@mui/material/styles';
import LinearProgress, {linearProgressClasses } from '@mui/material/LinearProgress';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import withAuth from '../../hoc/withAuth'
import SortIcon from '@mui/icons-material/Sort';

const UserProfile = () => {
    // States
    const [name, setName] = useState("")
    const [bio, setBio] = useState("")
    const [workStatus, setWorkStatus] = useState("")
    const [funFact, setFunFact] = useState("")
    const [inputMessage, setInputMessage] = useState("")
    const [successMessage, setSuccessMessage] = useState("")
    const [severity, setSeverity] = useState("")
    const [searchPostName, setSearchPostName] = useState("")
    const [posts, setPosts] = useState([])
    const [senderName, setSenderName] = useState([])

    const [newest, setNewest] = useState(true)
    const [open, setOpen] = useState(false);
    const [confirm, setConfirm] = useState(false);

    const [progress, setProgress] = useState(0);
    const [colorBar, setColorBar] = useState("")

    const {user, setUser} = useUser();

    useEffect(() => {
        let abortController = new AbortController();
        let userArr = []
        setName(user.name)
        setBio(user.bio)
        setWorkStatus(user.workStatus)
        setFunFact(user.funFact)
        
        // Fetches the necessary data
        const fetchData = async () => {
            userArr.length = 0
            const response = await getDirectPostToUser(user.id)  // Get all the direct posts from user
            setPosts(response.data) // Set the posts
            
            response.data.forEach(async (s) => {
                const response = await getSpecificUser(s.senderId); // Get the specific user based on the senderId from post
                if(response.id === s.senderId){
                    userArr.push(response)   // Add to array
                    const composed = removeDuplicateObjectFromArray(userArr, 'name') // remove duplicates
                    setSenderName(composed) // Set the states         
                }
            })
        }

        fetchData()

        // Checking inital user values
        // none filled
        if(user.bio === "" && user.workStatus === "" && user.funFact === ""){
            setProgress(25)
            setColorBar("#d85a5a")
        }
        // one filled
        if(user.bio === "" && user.workStatus === "" && user.funFact !== ""){
            setProgress(50)
            setColorBar("#ddee47")
        }
        if(user.bio === "" && user.workStatus !== "" && user.funFact === ""){
            setProgress(50)
            setColorBar("#ddee47")
        }
        if(user.bio !== "" && user.workStatus === "" && user.funFact === ""){
            setProgress(50)
            setColorBar("#ddee47")
        }
        // Two filled
        if(user.bio !== "" && user.workStatus !== "" && user.funFact === ""){
            setProgress(75)
            setColorBar("#e79f32")
        }
        if(user.bio !== "" && user.workStatus === "" && user.funFact !== ""){
            setProgress(75)
            setColorBar("#e79f32")
        }
        if(user.bio === "" && user.workStatus !== "" && user.funFact !== ""){
            setProgress(75)
            setColorBar("#e79f32")
        }
        // All filled
        if(user.bio !== "" && user.workStatus !== "" && user.funFact !== ""){
            setProgress(100)
            setColorBar("#34c44c")
        }

        return () => {  
            abortController.abort();  
        }  
    }, [user.bio, user.funFact, user.name, user.picture, user.workStatus, user.id])

    // Removes duplicates from array
    const removeDuplicateObjectFromArray = (array, key) => {
        var check = new Set();
        return array.filter(obj => !check.has(obj[key]) && check.add(obj[key]));
    }

    // Helper for controlling the user bar
    const checkForProfileValues = () => {
        // none filled
        if(bio === "" && workStatus === "" && funFact === ""){
            setProgress(25)
            setColorBar("#d85a5a")
        }
        // one filled
        if(bio === "" && workStatus === "" && funFact !== ""){
            setProgress(50)
            setColorBar("#ddee47")
        }
        if(bio === "" && workStatus !== "" && funFact === ""){
            setProgress(50)
            setColorBar("#ddee47")
        }
        if(bio !== "" && workStatus === "" && funFact === ""){
            setProgress(50)
            setColorBar("#ddee47")
        }
        // Two filled
        if(bio !== "" && workStatus !== "" && funFact === ""){
            setProgress(75)
            setColorBar("#e79f32")
        }
        if(bio !== "" && workStatus === "" && funFact !== ""){
            setProgress(75)
            setColorBar("#e79f32")
        }
        if(bio === "" && workStatus !== "" && funFact !== ""){
            setProgress(75)
            setColorBar("#e79f32")
        }
        // if(bio !== "" && workStatus !== "" && funFact === ""){
        //     setProgress(75)
        //     setColorBar("#e79f32")
        // }
        // All filled
        if(bio !== "" && workStatus !== "" && funFact !== ""){
            setProgress(100)
            setColorBar("#34c44c")
        }
    }

    // Updates the user information
    const updateUserInformation = async () => {
        // Name cant be empty
        if(name === ""){
            setInputMessage("Your name can't be empty")
        }else{
            setInputMessage("") 

            // Format the object
            const patchedData = {
                id: user.id,
                name: name,
                email: user.email,
                picture: "",
                workStatus: workStatus,
                bio: bio,
                funFact: funFact
            }

            const response = await patchUser(patchedData);
            
            // Checking the response, success or error, base the popup on response message
            if(response.status === 204){
                setOpen(true);
                setUser(patchedData)
                setName(patchedData.name)
                setBio(patchedData.bio)
                setWorkStatus(patchedData.workStatus)
                setFunFact(patchedData.funFact)
                setSuccessMessage("")
                checkForProfileValues()
            }

            if(response.status !== 204){
                setSeverity("error")
            } 

            handleConfirmClose();
            checkForProfileValues()

        }
    }

    // Edited alert
    const Alert = forwardRef(function Alert(props, ref) {
        return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
    });

    // Handle popup close
    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
          return;
        }
    
        setOpen(false);
    };

    // Open the popup
    const handleOpen = () => {
        setConfirm(true)
    }

    // Handle confirmation screen
    const handleConfirmClose = () => {
        setConfirm(false);
    };

    // Progress bar - Material UI
    const BorderLinearProgress = styled(LinearProgress)(({ theme }) => ({
        height: 10,
        borderRadius: 5,
        [`&.${linearProgressClasses.colorPrimary}`]: {
          backgroundColor: theme.palette.grey[theme.palette.mode === 'light' ? 200 : 800],
        },
        [`& .${linearProgressClasses.bar}`]: {
          borderRadius: 5,
          backgroundColor: theme.palette.mode === 'light' ? `${colorBar}` : '#308fe8',
        },
      }));
    
    return (
        <div>
             {/* Welcomes the user with name and progress bar */}
            <Box sx={{width: '50%', float: "left",  marginBottom: 1}}>
                <Box sx={{marginTop: 3, marginBottom: 1}}>
                    <Typography sx={{fontSize: 30}}>Välkommen {user.name}</Typography>
                    <Tooltip title="Profile completion" placement='bottom'>
                        <Box sx={{ display: 'flex', flexGrow: 1,alignItems: 'center', marginTop: 1 }}>
                            <Box sx={{ width: '83%', mr: 1 }}>
                                <BorderLinearProgress variant="determinate" value={progress} sx={{marginLeft: 13}}/>
                            </Box>
                            <Typography variant="body2" color="text.secondary" sx={{fontSize: 11,}}>{progress}%</Typography>
                        </Box>
                    </Tooltip>
                        
                </Box>
                <Box
                    sx={{
                        display: 'flex',
                        alignItems: 'center',
                        flexDirection: 'column',
                        marginBottom: 10,
                    }}
                    >
                    {/* Textfields for editing the profile data */}
                    <Typography  sx={{color: 'green', marginBottom: 3}}>{successMessage}</Typography>
                    <Typography sx={{ color: 'lightcoral'}}>{inputMessage}</Typography>
                    <TextField helperText="Name" sx={{width: 400}} value={name} onChange={(e) => setName(e.target.value)}/>
                    <TextField helperText="Work status" sx={{width: 400}} value={workStatus} onChange={(e) => setWorkStatus(e.target.value)}/>
                    <TextField helperText="Biography" sx={{width: 400}} value={bio} onChange={(e) => setBio(e.target.value)}/>
                    <TextField helperText="Fun fact" sx={{width: 400}}  value={funFact} onChange={(e) => setFunFact(e.target.value)}/>
                    
                    <Button onClick={handleOpen} variant="contained">Update profile <UpdateIcon sx={{marginLeft: 1}}/></Button>
                </Box> 
            </Box>

            <Box sx={{width: '50%', float: "right",  marginBottom: 9}}>
                <Typography sx={{fontSize: 30, marginTop: 3}}>All direct messages</Typography>     
                <Box sx={{display: 'flex', justifyContent: 'space-evenly', marginTop: 1}}>                                                            
                    <TextField label="Search for a direct message" sx={{width: '77%'}} type="text" autoComplete="off" onChange={(e) => setSearchPostName(e.target.value)}/>
                     {/* Most recent posts/oldest */}
                    <Button variant="outlined" sx={{marginBottom: 3, marginTop: 1, fontSize: 13}}  onClick={() => setNewest(!newest)}>{newest ? <><SortIcon/>Recent</> : <><SortIcon/>Oldest</>}</Button>
                </Box>
                 {/* Search for posts */}
                {newest  ? 
                <>
                    {posts.filter((post) => {
                        if(searchPostName === ""){
                            return post
                        }
                        else if(post.title.toLowerCase().includes(searchPostName.toLowerCase())){
                            return post
                        }
                        return null;
                    }).map((post, i) => (
                        <div key={i}>
                            {post.length === 0 ? null : 
                            <Card sx={{minWidth: 275, marginTop: 2.5, marginRight: 1.5, marginBottom: 1.5, marginLeft: 1.5}} key={i}>
                                <Accordion>
                                    <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel1a-content"
                                    id="panel1a-header"
                                    sx={{display: 'flex', flexDirection: 'column'}}>
                                        <Box>
                                            <Typography variant="h6" component="div">
                                            <strong>Title: </strong>{post.title}
                                            </Typography>
                                            <Typography sx={{ fontSize: 14 }} color="text.primary" gutterBottom>
                                                <strong>Description:</strong> {post.description}
                                            </Typography>
                                            <Typography sx={{ fontSize: 14 }} color="text.primary" gutterBottom>
                                                <strong>Timestamp:</strong> {moment(post.timeStamp).format("yyyy/MM/DD - H:mma")}
                                            </Typography>
                                        </Box>
                                    </AccordionSummary>
                                    <AccordionDetails>
                                        {senderName.map((u, i) => (
                                            <>
                                                <div key={i}>
                                                {post.senderId === u.id ?
                                                    <Typography sx={{ fontSize: 14 }} >
                                                       <strong>Sender:</strong> {u.name} • {u.workStatus}
                                                    </Typography>
                                                : null}
                                                </div>
                                            </>
                                        ))}
                                    </AccordionDetails>
                                </Accordion> 
                            </Card>}
                            
                        </div>
                    ))}
                    </>
                : <>
                 {/* All the profiles */}
                    {posts.filter((post, i) => {
                        if(searchPostName === ""){
                            return post
                        }
                        else if(post.title.toLowerCase().includes(searchPostName.toLowerCase())){
                            return post
                        }
                        return null
                    }).map((post, i) => (
                        <div key={i}>
                            {post.length === 0 ? null : 
                            <Card sx={{minWidth: 275, marginTop: 2.5, marginRight: 1.5, marginBottom: 1.5, marginLeft: 1.5}} key={i}>
                                <Accordion>
                                    <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel1a-content"
                                    id="panel1a-header"
                                    sx={{display: 'flex', flexDirection: 'column'}}>
                                        <Box>
                                            <Typography variant="h6" component="div">
                                            <strong>Title: </strong>{post.title}
                                            </Typography>
                                            <Typography sx={{ fontSize: 14 }} color="text.primary" gutterBottom>
                                                <strong>Description:</strong> {post.description}
                                            </Typography>
                                            <Typography sx={{ fontSize: 14 }} color="text.primary" gutterBottom>
                                                <strong>Timestamp:</strong> {moment(post.timeStamp).format("yyyy/MM/DD - H:mma")}
                                            </Typography>
                                        </Box>
                                    </AccordionSummary>
                                    <AccordionDetails>
                                        {senderName.map((u) => (
                                            <div>
                                                {post.senderId === u.id ?
                                                    <Typography sx={{ fontSize: 14 }} >
                                                    <strong>Sender:</strong> {u.name} • {u.workStatus}
                                                    </Typography>
                                                : null}
                                            </div> 
                                        ))}
                                    </AccordionDetails>
                                </Accordion> 
                            </Card>}
                        </div>
                    )).reverse()}
                </>}
                
            
            </Box>
            {/* Alert */}
            {severity === "error" ? 
            
                <Snackbar open={open} autoHideDuration={5000} onClose={handleClose}>
                    <Alert severity="error">Error: Could not update the user profile!</Alert>
                </Snackbar> : 
                <Snackbar open={open} autoHideDuration={5000} onClose={handleClose}>
                    <Alert severity="success">Success: User profile updated!</Alert>
                </Snackbar>
            }

             {/* Dialog confirmation box */}
            <Dialog
                open={confirm}
                onClose={handleOpen}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
                >
                <DialogTitle id="alert-dialog-title">
                {"Confirmation"}
                </DialogTitle>
                <DialogContent>
                <DialogContentText id="alert-dialog-description">
                    Press no to go back, otherwise press yes to update your information.
                </DialogContentText>
                </DialogContent>
                <DialogActions>
                <Button onClick={handleConfirmClose}>No</Button>
                <Button onClick={() => updateUserInformation()}>
                    Yes
                </Button>
                </DialogActions>
            </Dialog>
        </div>
    )
}

export default withAuth(UserProfile);
import * as React from 'react';
import { useEffect, useState  } from "react";
import { createNewEvent, getEventsByUserId } from "../../api/eventEndpoints";
import { getAllPostsFromUser } from "../../api/postEndpoints";
import usePagination from "../../utils/Pagination";
import { Box, Card, Pagination, Typography } from "@mui/material";
import TimelinePost from "../../components/Timeline/TimelinePost";
import TimelineEvent from "../../components/Timeline/TimelineEvent";
import TextField from '@mui/material/TextField';
import { useUser } from "../../context/UserContext";
import { getGroupById } from "../../api/groupEndpoints";
import FullCalendar from '@fullcalendar/react'
import dayGridPlugin from '@fullcalendar/daygrid'
import interactionPlugin from '@fullcalendar/interaction'
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';
import moment from 'moment'
import { DateTimePicker } from '@mui/x-date-pickers';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faLongArrowRight } from '@fortawesome/free-solid-svg-icons'
import withAuth from '../../hoc/withAuth';

const Timeline = () => {

    //State declarations
    const [posts, setPosts] = useState([])
    const [stateEvents, setEvents] = useState([])
    const [postSearchValue, setPostSearchValue] = useState("")
    const [eventSearchValue, setEventSearchValue] = useState("")
    const [groups, setGroups] = useState([])
    const [open, setOpen] = useState(false);
    const [openEvent, setOpenEvent] = useState(false);
    const [stateCalendarEvents, setCalendarEvents] = useState({event: {title: "", start: new Date(), end: new Date()}});
    const { user } = useUser();
    const [openDay, setOpenDay] = useState(false);
    const [eventTitle, setEventTitle] = useState("");
    const [eventStartTime, setEventStartTime] = useState("");
    const [eventEndTime, setEventEndTime] = useState("");
    const [eventAllowedGuests, setEventAllowedGuests] = useState(0);
    const [eventDescription, setEventDescription] = useState("");
    const [postErrorFetch, setPostErrorFetch] = useState("")
    const [valid, setValid] = useState(false);

    //Array of groups
    let groupArray = []
    
    //Pagination for events and posts 
    let [postPage, setPostPage] = useState(1);
    let [eventPage, setEventPage] = useState(1);
    const POST_PER_PAGE = 3;
    const EVENT_PER_PAGE = 4;

    const post_count = Math.ceil(posts.filter(post => !post.replyId).length / POST_PER_PAGE);
    const {currentData, jump} = usePagination(posts, POST_PER_PAGE);


    const event_count = Math.ceil(stateEvents.length / EVENT_PER_PAGE);
    const EVENT_DATA = usePagination(stateEvents, EVENT_PER_PAGE);

    
    //Change post page
    const handlePostChange = (e, p) => {
        setPostPage(p);
        jump(p);
    };

    //Change event page
    const handleEventChange = (e, p) => {
        setEventPage(p);
        EVENT_DATA.jump(p);

    }

    //Method for only setting allowed guests to numbers
    const handleEventAllowedGuestsOnChange = (e) => {
        setEventAllowedGuests(e.target.value);
        const reg = new RegExp("[0-9]+");
        setValid(reg.test(e.target.value));
    }

    //Sets open to true, opens dialog
  const handleClickOpen = () => {
    setOpen(true);
  };

  //Sets open to false, closes dialog
  const handleClose = () => {
    setOpen(false);
  };
  
  //Open event
  const handleEventClickOpen = ({event}) => {
      setOpenEvent(true);
      setCalendarEvents({event})
      
    }

    //Close event
  const handleEventClickClose = () => {
    setOpenEvent(false);
  }

  //Opens day on click in calendar
  const handleDayOpen = (date) => {
    setOpenDay(true);
    setEventStartTime(date.dateStr)
  }

  //Closes day modal
  const handleDayOpenClose = () => {
    setOpenDay(false)
  }

  
//Creates new event on click
  const onSubmit = async () => {
    
        const eventData = {
            startTime: eventStartTime,
            description: eventDescription,
            name: eventTitle,
            allowedGuests: eventAllowedGuests,
            endTime: eventEndTime,
            userId: user.id,
            bannerImg: ""
        }
        
        await createNewEvent(eventData);
        window.location.reload()
    
    }

    //Sets events for calendar
    const calendarEvents = () => {
        let events = stateEvents.map(event => (
            {
                title: event.name, 
                start: moment(event.startTime).toISOString(), 
                end: moment(event.endTime).toISOString(),
            }
        ))
        return events;
    }

    useEffect(() => {
        let abortController = new AbortController();
        //Fetches data 
        const fetchData = async () => {
            await getAllPostsFromUser(user.id).then((response) => {
                if(response.status === 200){
                    const composed = response.data.map(d => {
                        return {
                            ...d,
                            groups: groups.filter(({id}) => d.groupId === id)
                        }
                    })
                    setPosts(composed)
                }
                if(response === 404){
                    setPostErrorFetch("You have no posts!")
                }
            }); 

            const eventResponse = await getEventsByUserId(user.id);
            setEvents(eventResponse.data)

            //Loops through posts and gets group from post by id
                posts.forEach(async post => {
                    getGroupById(post.groupId).then((response) => {
                        groupArray.push(response) //response.data
                        const composed = posts.map(d => {
                            return {
                                ...d,
                                groups: groupArray.filter((id) => d.groupId === id).map(x => x.name).filter((v, i, a) => a.indexOf(v) === i) //filter(({id}))
                            }
                        })
                    var groupsArray = composed.map(x => x.groups)
                    setGroups(groupsArray)
                    setPosts(composed.sort(function(a,b) {
                        return new Date(b.timeStamp) - new Date(a.timeStamp)
                    }))
                    })
                    

                }) 
        }
        fetchData();

        return () => {  

            abortController.abort();  

        } 
    }, [posts.some(x => x.groups)])

    return(
        <>      

                <div className="timeline">
                    <div style={{borderRight: "1px solid #E7E7E7", padding: 20}}>  
                    {/* Searchfield for posts */}
                        <TextField label="Search Posts" variant="outlined" style={{width: 600}} onChange={(event) => {
                            setPostSearchValue(event.target.value)
                            }}/>
                            <Typography>{postErrorFetch}</Typography>
                            {/* Paginated data filter */}
                            {currentData().filter((post, i) => {
                                if(postSearchValue === ''){
                                    return  <>
                                                {!post.replyId ? <TimelinePost posts={post} groups={post.groups} key={i}/> : null }
                                            </>
                                } else if(post.title.toLowerCase().includes(postSearchValue.toLowerCase() ||
                                post.postReplies.filter(postReply => postReply.title.toLowerCase().includes(postSearchValue.toLowerCase()))) 
                                ){
                                    return  <>
                                                {!post.replyId ? <TimelinePost posts={post} groups={post.groups} key={i}/> : null }
                                            </>
                                }
                                return null
                            })
                            .map((post, i) => 
                                <div key={i}>
                                    {!post.replyId ? <TimelinePost posts={post} groups={post.groups} key={post.id}/> : null }
                                </div>
                            )}
                            {/* Pagination for post */}
                            <div>
                                <Pagination
                                count={post_count}
                                size="large"
                                page={postPage}
                                variant="outlined"
                                shape="rounded"
                                onChange={handlePostChange}
                                style={{display: "flex", flexDirection: "row", justifyContent: "center"}}
                                />
                            </div>
                        
                    </div>
                    <div style={{padding: 20, borderLeft: "1px solid #E7E7E7"}}>
                        <div style={{display: "flex", flexDirection:"row"}}>
                            <TextField label="Search Events" variant="outlined" style={{width: 400}} onChange={event => setEventSearchValue(event.target.value)}/>
                            <Button variant="outlined" onClick={() => handleClickOpen()}>
                                <CalendarMonthIcon/>
                            </Button>
                        </div>
                        {/* Paginated data for events */}
                        {EVENT_DATA.currentData().filter(event => {
                            if(eventSearchValue === ''){
                                return event;
                            } else if (event.name.toLowerCase().includes(eventSearchValue.toLowerCase())){
                                return event;
                            }
                            return null
                        }).map((event, i) => (
                            <TimelineEvent events={event} key={i}/>
                        ))}
                        <div >
                            {/* Pagination for event */}
                            <Pagination
                                count={event_count}
                                size="large"
                                page={eventPage}
                                variant="outlined"
                                shape="rounded"
                                onChange={handleEventChange}
                                style={{
                                display: "flex", 
                                flexDirection: "row",
                                justifyContent: "center",
                                }}/>
                        </div>
                    </div>
                    
            </div>
            <div >
                {/* Dialog for calendar */}
                <Dialog
                open={open}
                keepMounted
                onClose={handleClose}
                aria-describedby="alert-dialog-slide-description"
                fullWidth
                maxWidth="lg"
                style={{backgroundColor: "#1976d2"}}
                >
                <DialogTitle>{"Check out your calendar"}</DialogTitle>
                    <DialogContent>
                        <FullCalendar
                        plugins={[ dayGridPlugin, interactionPlugin ]}
                        initialView="dayGridMonth"
                        events={calendarEvents()}
                        eventClick={handleEventClickOpen}
                        dateClick={handleDayOpen}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleClose}>Close</Button>
                    </DialogActions>
                </Dialog>
            </div>

            <div>
                {/* Dialog to for event */}
            <Dialog
                open={openEvent}
                keepMounted
                onClose={handleEventClickClose}
                aria-describedby="alert-dialog-slide-description"
                maxWidth="md"
                >
                    <DialogTitle>
                        {stateCalendarEvents.event.title}
                    </DialogTitle>
                    <DialogContent >
                        <Card elevation={3} 
                        style={{
                            display:"flex", 
                            flexDirection:"row", 
                            justifyContent:"space-between",
                            padding: "20",
                            minWidth: "60vw"}}>
                            <p style={{marginLeft: "2vw"}}> <span style={{fontWeight: "bold"}}>Started at</span>  {moment(stateCalendarEvents.event.start).format('yyyy/MM/DD - H:mma')}</p>
                            <p><FontAwesomeIcon icon={faLongArrowRight} /></p> 
                            <p style={{marginRight: "2vw"}}> <span style={{fontWeight: "bold"}}>Ends at</span>  {moment(stateCalendarEvents.event.end).format('yyyy/MM/DD - H:mma ') }</p>
                        </Card>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleEventClickClose}>Close</Button>
                    </DialogActions>
            </Dialog>
            </div>
            <div>
                {/* Dialog to open day and create event on click day in calendar */}
            <Dialog
                open={openDay}
                keepMounted
                onClose={handleDayOpenClose}
                aria-describedby="alert-dialog-slide-description"
                maxWidth="lg"
                >
                    <DialogTitle>
                        <p>Create an event</p>
                    </DialogTitle>
                    <DialogContent style={{minHeight: "50vh"}}>
                        <Box
                        sx={{
                          display: "flex",
                          flexDirection: "column",
                          justifyContent: "center",
                          alignItems: "center"
                        }}>
                            <div className="dialog-content-selected-day-calendar" 
                            style={{
                            display:"flex", 
                            justifyContent:"center", 
                            flexDirection:"column", 
                            padding: 30}}>
                                <div style={{marginBottom: "2vh"}}>
                                    <TextField style={{marginRight: "1vw"}} label="Event Title" variant="outlined" onChange={(e) => setEventTitle(e.target.value)}
                                    error={!valid}
                                    required={true}/>
                                    <TextField style={{marginRight: "1vw"}} label="Start time" variant="outlined" value={eventStartTime} disabled type="date"/>
                                </div>
                                <div>
                                    <LocalizationProvider dateAdapter={AdapterDayjs}>
                                        <DateTimePicker style={{marginRight: "1vw"}} label="End time" onChange={(date) => setEventEndTime(date)}
                                        renderInput={(params) => <TextField {...params}/>}
                                        value={eventEndTime}
                                        minDate={eventStartTime}
                                        />
                                    </LocalizationProvider>
                                    <TextField style={{marginRight: "1vw"}} label="Allowed Guests" variant="outlined" onChange={(e) => handleEventAllowedGuestsOnChange(e)} 
                                    inputProps={{ pattern: "[0-9]+" }}
                                    error={!valid}
                                    required={true}/>
                                </div>
                                <div>
                                <TextField
                                    style={{margin: "0 auto", width:"100%", marginTop:"1vh"}}
                                    id="outlined-multiline-flexible"
                                    label="Event Description"
                                    multiline
                                    rows={4}
                                    onChange={(e) => setEventDescription(e.target.value)}
                                    />
                                </div>
                                <div>
                                    <Button onClick={() => onSubmit()}>Click to create event</Button>
                                </div>
                            </div>
                        </Box>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleDayOpenClose}>Close</Button>
                    </DialogActions>
            </Dialog>
            </div>
        </>
    )
}

export default withAuth(Timeline);
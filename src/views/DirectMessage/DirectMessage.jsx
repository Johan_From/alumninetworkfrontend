import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import MuiAlert from '@mui/material/Alert';
import { forwardRef, useEffect, useState } from "react"
import { createPost } from "../../api/postEndpoints";
import { getAllUsers } from "../../api/userEndpoints";
import { Button, Snackbar, Typography } from '@mui/material';
import { useUser } from '../../context/UserContext'
import SendIcon from '@mui/icons-material/Send';
import withAuth from '../../hoc/withAuth';

const DirectMessage = () => {
    // States
    const {user} = useUser();
    const [users, setUsers] = useState([])
    const [pickedUser, setPickedUser] = useState("")
    const [userToSend, setUserToSend] = useState("")

    const [title, setTitle] = useState("");
    const [description, setDescription] = useState("");

    const [inputMessage, setInputMessage] = useState("");

    const [severity, setSeverity] = useState("")
    const [open, setOpen] = useState(false)

    const [searchValue, setSearchValue] = useState("")

    useEffect(() => {
        let abortController = new AbortController();
        // Fetch all the users
        const fetchUsers = async () => {
            const response = await getAllUsers();
            setUsers(response.data)
        }
        fetchUsers();

        return () => {
            abortController.abort();
        }
    }, [])


    // When sending a message
    const onSubmit = async () => {
        // Check so the fields are not empty
        if(title === "" || description === "" || pickedUser.id < 1){
            setInputMessage("You must select a user to send to, and fill the fields with values")
        } else{
            setInputMessage(null)

            // Format object
            const currentDate = new Date().toISOString();
            const postData = {
                timeStamp: currentDate,
                title: title,
                description: description,
                senderId: user.id,
                userId: userToSend,
                groupId: 0,
                eventId: 0,
                topicId: 0,
                replyId: 0
            }
            
            const response = await createPost(postData);

            // Handle resposnes
            if(response.status === 200){
                setSeverity("success")
                setOpen(true)
            }

            if(response.status !== 200){
                setSeverity("error")
                setOpen(true)
            }
         }
    }

    // Helper for setting the values for the user
    const setUserReciever = (pickedUser) => {
        setUserToSend(pickedUser.id)
        setPickedUser(`${pickedUser.name}: ${pickedUser.workStatus}`)
        setSearchValue("")
    }

    // Alert
    const Alert = forwardRef(function Alert(props, ref) {
        return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
    });


    // Handle alert close
    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
          return;
        }
    
        setOpen(false);
    };

    return(
        <div>
            <Typography variant="h4" sx={{marginTop: 2}}>Send a message to a user</Typography>
            <Typography  sx={{color: 'red', marginBottom: 3}}>{inputMessage}</Typography>
            <TextField label="Receiver (name)" sx={{width: 400}} type="text" autoComplete="off" placeholder="Search for user" onChange={(e) => setSearchValue(e.target.value)} />
             {/* Search for user*/}
            {users.filter((u) => {
                if(searchValue === ""){
                    return u
                }
                else if(u.name.toLowerCase().includes(searchValue.toLowerCase())){
                    return u
                }
                return null
            }).map((u) => (
                <div>
                    {searchValue === "" 
                    ? null 
                    : <>
                        <br />
                         {/* Users to pick from*/}
                        <Button sx={{marginTop: 0.5}} onClick={() => setUserReciever(u)}>{u.name}: {u.workStatus}</Button>
                    </>
                    }
                     
                </div>
                
            ))}

            <div>
                 {/* Textfields*/}
                <Box sx={{
                        display: 'flex',
                        alignItems: 'center',
                        flexDirection: 'column',
                        marginTop: 2,
                        marginBottom: 20,
                        '& > :not(style)': { m: 1 },
                    }}>
                    <TextField value={pickedUser} disabled="true" sx={{width: 400}}/>
                    <TextField label="Title"  sx={{width: 400}} onChange={(e) => setTitle(e.target.value)}/>
                    <TextField label="Description" sx={{width: 400}} onChange={(e) => setDescription(e.target.value)}/>
                    <Button variant="contained" onClick={() => onSubmit()}>Send message <SendIcon sx={{marginLeft: 1}}/></Button>
                </Box> 
            </div>
            
             {/* Alert*/}
            {severity === "error" ? 
                <Snackbar open={open} autoHideDuration={5000} onClose={handleClose}>
                    <Alert severity="error">Could not send the message!</Alert>
                </Snackbar> : 
                <Snackbar open={open} autoHideDuration={5000} onClose={handleClose}>
                    <Alert severity="success">Message sent successfully!</Alert>
                </Snackbar>
            }
        </div>
    )
}

export default withAuth(DirectMessage);
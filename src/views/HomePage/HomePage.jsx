import React from 'react';
import keycloak from '../../keycloak';
import Button from '@mui/material/Button';
import { useState } from 'react';
import { Box, Card, Typography } from '@mui/material';
import LinearProgress from '@mui/material/LinearProgress';


const HomePage = () => {
    //const navigate = useNavigate()
    const [loading, setLoading] = useState(false)
    const handleLogin = async () => {
        setLoading(true)
        await keycloak.login();
        setLoading(false)
    }
    return (
        <div>
            {loading ? <LinearProgress /> : null}
                {/* Homepage card */}
                <Card elevation={3} sx={{display: 'flex', justifyContent: 'center', marginRight: '20%', marginLeft: '20%', marginTop: '2%', marginBottom: '2%'}}>
                    <Box sx={{marginTop: 3, marginBottom: 3}}>
                        <img src="firstPageImage.jpg" style={{width: 270, border: 'solid 1px lightgrey', borderRadius: 10, boxShadow: '1px 2px 9px lightgrey'}} alt="start_image"/>
                    </Box>
                    <Box sx={{marginTop: '11%', width: '50%', marginLeft: '3%'}}>
                        <Typography sx={{borderBottom: "solid 1px lightgrey"}} variant='h4'>Welcome to AlumniNetwork</Typography>
                        <Typography>The online networks for alumines!</Typography>
                        <Typography sx={{borderBottom: "solid 1px lightgrey"}}>Please press the login button below to start using the application</Typography>
                        <Button sx={{marginTop: 2}} variant="contained" onClick={() => handleLogin()}>Login</Button>
                        {loading ? <Typography sx={{marginTop: 1, color: 'lightgrey'}}>Waiting for redirect...</Typography> : null}
                    </Box>
                </Card>
        </div>
    );
};

export default HomePage;
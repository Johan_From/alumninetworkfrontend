import { Box, Typography } from "@mui/material"
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import Avatar from '@mui/material/Avatar';
import MessageIcon from '@mui/icons-material/Message';
import GroupsIcon from '@mui/icons-material/Groups';
import EventIcon from '@mui/icons-material/Event';
import TopicIcon from '@mui/icons-material/Topic';
import AccountBoxIcon from '@mui/icons-material/AccountBox';
import LanguageIcon from '@mui/icons-material/Language';
import NotesIcon from '@mui/icons-material/Notes';
import StorageIcon from '@mui/icons-material/Storage';
import CloudIcon from '@mui/icons-material/Cloud';

const About = () => {
    return(
        <Box>
            <Typography variant="h4" sx={{marginTop: 1, marginBottom: 1}}>About AlumniNetwork</Typography>
            <Box sx={{width: '50%', float: "left",  marginBottom: 9}}>

                <Typography variant="h6" sx={{marginTop: 1}}>Functionalities</Typography>
                <List sx={{marginLeft: 22}}>
                    <ListItem>
                        <ListItemAvatar>
                        <Avatar sx={{ backgroundColor: 'lightblue'}}>
                            <MessageIcon />
                        </Avatar>
                        </ListItemAvatar>
                        <ListItemText primary="Send messages to other alumni" />
                    </ListItem>
                    <ListItem>
                        <ListItemAvatar>
                        <Avatar sx={{ backgroundColor: 'lightblue'}}>
                            <GroupsIcon />
                        </Avatar>
                        </ListItemAvatar>
                        <ListItemText primary="See, join and post in groups"/>
                    </ListItem>
                    <ListItem>
                        <ListItemAvatar>
                        <Avatar sx={{ backgroundColor: 'lightblue'}}>
                            <EventIcon />
                        </Avatar>
                        </ListItemAvatar>
                        <ListItemText primary="Get invited to events" />
                    </ListItem>
                    <ListItem>
                        <ListItemAvatar>
                        <Avatar sx={{ backgroundColor: 'lightblue'}}>
                            <TopicIcon />
                        </Avatar>
                        </ListItemAvatar>
                        <ListItemText primary="Subscribe to topics" />
                    </ListItem>
                    <ListItem>
                        <ListItemAvatar>
                        <Avatar sx={{ backgroundColor: 'lightblue'}}>
                            <AccountBoxIcon />
                        </Avatar>
                        </ListItemAvatar>
                        <ListItemText primary="Create and edit your profile" />
                    </ListItem>
                </List>
            </Box>
            <Box sx={{width: '50%', float: "left",  marginBottom: 9}}>
                <Typography variant="h6" sx={{marginTop: 1}}>The Tech Stack</Typography>
                <List sx={{marginLeft: 22}}>
                    <ListItem>
                        <ListItemAvatar>
                        <Avatar sx={{ backgroundColor: 'lightblue'}}>
                            <LanguageIcon />
                        </Avatar>
                        </ListItemAvatar>
                        <ListItemText primary="Languages" secondary="C# and JavaScript" />
                    </ListItem>
                    <ListItem>
                        <ListItemAvatar>
                        <Avatar sx={{ backgroundColor: 'lightblue'}}>
                            <NotesIcon />
                        </Avatar>
                        </ListItemAvatar>
                        <ListItemText primary="C#" secondary="ASP.NET Web API and Entity Framework "/>
                    </ListItem>
                    <ListItem>
                        <ListItemAvatar>
                        <Avatar sx={{ backgroundColor: 'lightblue'}}>
                            <NotesIcon />
                        </Avatar>
                        </ListItemAvatar>
                        <ListItemText primary="JavaScript" secondary="React, Axios, Material UI with more..." />
                    </ListItem>
                    <ListItem>
                        <ListItemAvatar>
                        <Avatar sx={{ backgroundColor: 'lightblue'}}>
                            <StorageIcon />
                        </Avatar>
                        </ListItemAvatar>
                        <ListItemText primary="Database" secondary="SQL Server" />
                    </ListItem>
                    <ListItem>
                        <ListItemAvatar>
                        <Avatar sx={{ backgroundColor: 'lightblue'}}>
                            <CloudIcon />
                        </Avatar>
                        </ListItemAvatar>
                        <ListItemText primary="Hosting" secondary="Backend: Azure and Frontend: Heroku" />
                    </ListItem>
                </List>
            </Box>
        </Box>
    )   
}

export default About;
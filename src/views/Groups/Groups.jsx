import { Accordion, AccordionDetails, AccordionSummary, Box, Button, Snackbar, TextField, Typography } from "@mui/material";
import { forwardRef, useEffect, useState } from "react";
import { getAllGroupsWithUser, joinNewGroup } from "../../api/groupEndpoints";
import { useUser } from "../../context/UserContext";
import MuiAlert from '@mui/material/Alert';
import GroupIcon from '@mui/icons-material/Group';
import GroupAddIcon from '@mui/icons-material/GroupAdd';
import { useNavigate } from "react-router-dom";
import AddGroupButton from "../../components/Buttons/AddGroupButton";
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import withAuth from "../../hoc/withAuth";


const Groups = () => {
    const [groups, setGroups] = useState([])
    const {user} = useUser();
    const [severity, setSeverity] = useState("")
    const [open, setOpen] = useState(false)
    const [groupName, setGroupName] = useState("")
    const [joinedGroupSearchValue, setJoinedGroupSearchValue] = useState("")
    const [joinableGroupSearchValue, setJoinableGroupSearchValue] = useState("")
    const navigate = useNavigate()

    useEffect(() => {
        let abortController = new AbortController();
        // Fetch all the groups
        const fetchData = async () => {
            const response = await getAllGroupsWithUser();
            setGroups(response.data)
            
        }
        fetchData();
        return () => {  
            abortController.abort();  
        }  
    }, [])
    
    // Relative URL method for getting to a specific group
    const redirectToGroup = async (group) => {
        var thisGroup = group.id;
        navigate(`/groups/${thisGroup}`)
    }

    // Method for joining a group
    const joinGroup = async (group) => {
        const response = await joinNewGroup(group.id, user.id)

        // Checking the response 
        if(response.status === 200){
            setSeverity("success")
            setGroupName(group.name)
            setOpen(true)
            const updated = await getAllGroupsWithUser();
            setGroups(updated.data)
        }

        if(response.status !== 200){
            setSeverity("error")
            setGroupName(group.name)
            setOpen(true)
        }
    }

    // Popup alert
    const Alert = forwardRef(function Alert(props, ref) {
        return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
    });

    // Handle popup close
    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
          return;
        }
    
        setOpen(false);
    };

    return(
        <div style={{display: 'flex', justifyContent: 'center', borderTop: "1px solid #E7E7E7"}}>
             {/* Displaying all the groups */}
            <Box sx={{marginBottom: 4, marginRight: 4, width: '40%', borderRight: "1px solid #E7E7E7"}}> 
                <Box sx={{ marginRight: 1}}>
                    <Typography sx={{fontSize: 27, marginBottom: 1.5, marginTop: 1.2, marginRight: 1}}>Joined groups</Typography>
                    <TextField label="Search joined groups" variant="outlined" sx={{width: '100%', marginBottom: 2}} onChange={event => setJoinedGroupSearchValue(event.target.value)}/>
                </Box>
                {groups.filter((group) => {
                    if(joinedGroupSearchValue === ""){
                        return group;
                    }
                    else if(group.name.toLowerCase().includes(joinedGroupSearchValue.toLowerCase())){
                        return group
                    }
                    return null;
                }).map((group, i) => (
                    <div key={i} style={{marginRight: 10, marginBottom: 10, marginTop: 10}}>
                        {/* Joined groups */}
                        {group.users.includes(user.id) ? 
                        <Accordion elevation={3} >
                            <AccordionSummary
                                expandIcon={<ExpandMoreIcon />}
                                aria-controls="panel1a-content"
                                id="panel1a-header"
                                sx={{display: 'flex', flexDirection: 'column'}}>
                                <Box>
                                    <Typography variant="h5" component="div" sx={{borderBottom: "1px solid #E7E7E7"}} >
                                        {group.name}
                                     </Typography>
                                     <Typography sx={{ fontSize: 14 }} color="text.primary" gutterBottom>
                                        {group.description}
                                     </Typography>
                                     <Typography sx={{ fontSize: 14 }} color="text.primary" gutterBottom>
                                        <strong>Members •</strong> {group.users.length}
                                     </Typography>
                                     <Typography sx={{ fontSize: 14 }} color="text.primary" gutterBottom>
                                        <strong>Publicity •</strong> {group.isPrivate === true ? "Private" : "Public"}
                                     </Typography>
                                </Box>
                            </AccordionSummary>
                            <AccordionDetails>
                            <Button variant="contained" onClick={() => redirectToGroup(group)}>Go to group <GroupIcon sx={{fontSize: 20, marginLeft: 1, marginBottom: 0.2}}/></Button>
                            </AccordionDetails>
                        </Accordion>
                    : null}
                    </div>
                ))}
            </Box>
            <Box sx={{marginBottom: 9, marginLeft: 6, borderLeft: "1px solid #E7E7E7", width: '35%'}} >
                <Typography sx={{fontSize: 27, marginBottom: 1.5, marginTop: 1.2}}>Joinable groups</Typography>
                <TextField label="Search joinable groups" variant="outlined" sx={{width: '100%', marginBottom: 2, marginLeft: 1}} onChange={event => setJoinableGroupSearchValue(event.target.value)}/>
                
                {groups.filter((group) => {
                    if(joinableGroupSearchValue === ""){
                        return group;
                    }
                    else if(group.name.toLowerCase().includes(joinableGroupSearchValue.toLowerCase())){
                        return group
                    }
                    return null;
                }).map((group, i) => (
                    <div key={i} style={{marginLeft: 10, marginBottom: 10}}>
                        {/* Joinable groups */}
                    {!group.users.includes(user.id) && group.isPrivate !== true ? 
                        <Accordion elevation={3}>
                        <AccordionSummary
                            expandIcon={<ExpandMoreIcon />}
                            aria-controls="panel1a-content"
                            id="panel1a-header"
                            sx={{display: 'flex', flexDirection: 'column'}}>
                            <Box>
                                <Typography variant="h5" component="div" sx={{borderBottom: "1px solid #E7E7E7"}}>
                                    {group.name}
                                </Typography>
                                <Typography sx={{ fontSize: 14 }} color="text.primary" gutterBottom>
                                    {group.description}
                                </Typography>
                                <Typography sx={{ fontSize: 14 }} color="text.primary" gutterBottom>
                                    <strong>Members •</strong> {group.users.length}
                                </Typography>
                                <Typography sx={{ fontSize: 14 }} color="text.primary" gutterBottom>
                                    <strong>Publicity •</strong> {group.isPrivate === true ? "Private" : "Not private"}
                                </Typography>
                            </Box>
                        </AccordionSummary>
                        <AccordionDetails>
                            <Button variant="contained" onClick={() => joinGroup(group)}>Join group <GroupAddIcon sx={{fontSize: 20, marginLeft: 1, marginBottom: 0.2}}/></Button>
                        </AccordionDetails>
                    </Accordion>
                    : null}
                    </div>
                ))}
                

            </Box>
            {severity === "error" ? 
                <Snackbar open={open} autoHideDuration={5000} onClose={handleClose}>
                    <Alert severity="error">Could not join group {groupName}!</Alert>
                </Snackbar> : 
                <Snackbar open={open} autoHideDuration={5000} onClose={handleClose}>
                    <Alert severity="success">Joined group {groupName}!</Alert>
                </Snackbar>
            }

            <AddGroupButton/>
        </div>
    )
}

export default withAuth(Groups);
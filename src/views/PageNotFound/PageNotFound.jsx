import { Typography } from "@mui/material";
import { Box } from "@mui/system";

const PageNotFound = () => {
    return (
        <Box>
            <Typography variant="h4">404</Typography>
            <Typography variant="h5">Could not find the page you are looking for</Typography>
        </Box>
    )
}

export default PageNotFound;
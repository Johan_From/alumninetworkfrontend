import { useEffect, useState } from "react";
import { getEventsBySubscription, getEventsByUserId } from "../../api/eventEndpoints";
import { getAllGroupsWithUser } from "../../api/groupEndpoints";
import { useUser } from "../../context/UserContext";
import moment from "moment";
import withAuth from "../../hoc/withAuth";
import { Card, Typography } from "@mui/material";
import { Box } from "@mui/system";
import SpecificEvent from "../../components/Events/SpecificEvent";

const Events = () => {
    
    // States for handling users and events
    const {user} = useUser();
    const [userEvents, setUserEvents] = useState([]);
    const [events, setEvents] = useState([]);
    const [groups, setGroups] = useState([]);
    
    useEffect(() => {

        const fetchEvents = async () => {
            // Fetches events from topics and groups which the user are subscribed to.
            const response = await getEventsBySubscription(user.id);
            // Fetches all groups the user have joined
            const fetchGroups = await getAllGroupsWithUser(user.id);
            // Fetches events the user have created
            const fetchEvents = await getEventsByUserId(user.id);
            setEvents(response.data)
            setGroups(fetchGroups.data);
            setUserEvents(fetchEvents.data)

        };
        fetchEvents();
    }, [])

    // Maps out the events
    const groupEventRender = () => {
        return (
            events.map((event, i) => {
            if(event.groups.length > 0){
                return(
                    <Card key={i} elevation={3} sx={{marginBottom: 2, padding: 2}}>
                        <h2 >{event.name}</h2>
                        <p>Invited guests: {event.invitedGuests}</p>
                        <p>Allowed guests: {event.allowedGuests}</p>
                        <p>Event Starts: {moment(event.startTime).format('yyyy/MM/DD - H:mma')}</p>
                        <p>Event Ends: {moment(event.endTime).format('yyyy/MM/DD - H:mma')}</p>
                    </Card>
                )   
            }
        }))
    }

    return(
        <>
            <Typography variant="h4" sx={{marginTop: 3, paddingBottom: 2, borderBottom: 'solid 1px #E7E7E7'}}>Events</Typography>
            <Box sx={{display: 'flex', justifyContent: 'center'}}>
                <Box sx={{borderRight: 'solid 1px #E7E7E7', paddingRight: 3, marginRight: 3, paddingTop: 2}}>
                    <Typography variant="h5">Group events!</Typography>
                    {groupEventRender()}
                </Box>

                <Box sx={{borderLeft: 'solid 1px #E7E7E7', paddingLeft: 3, marginLeft: 10, paddingTop: 2}}>
                    <Typography variant="h5">Events Created By {user.name}</Typography>
                    {/* {eventCreatedByUser()} */}
                    {userEvents.map((e, i) => (
                        <div key={i}>
                        {e.userId === user.id ?
                            
                            <SpecificEvent key={i} event={e}/>

                        : null}
                        </div>
                    ))}
                </Box>
            </Box>
        </>
    )
}

export default withAuth(Events);
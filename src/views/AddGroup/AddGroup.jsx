import { forwardRef, useState } from "react";
import { useUser } from "../../context/UserContext";
import MuiAlert from '@mui/material/Alert';
import FormControl from '@mui/material/FormControl';
import { Box, Button, InputLabel, MenuItem, Select, Snackbar, TextField, Tooltip, Typography } from "@mui/material";
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { createNewGroup } from "../../api/groupEndpoints";
import QuestionMarkIcon from '@mui/icons-material/QuestionMark';
import withAuth from "../../hoc/withAuth";

const AddGroup = () => {
    // States
    const {user} = useUser()
    const [title, setTitle] = useState("");
    const [description, setDescription] = useState("");;
    const [inputMessage, setInputMessage] = useState("");
    const [severity, setSeverity] = useState("")
    const [open, setOpen] = useState(false)

    const [status, setStatus] = useState("")
    const [confirm, setConfirm] = useState(false);

    // Handle conformation box close
    const handleConfirmClose = () => {
        setConfirm(false);
    };


    // Alert edit
    const Alert = forwardRef(function Alert(props, ref) {
        return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
    });

    // Handle popup close
    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
          return;
        }
    
        setOpen(false);
    };

    // Handle the confirmation popup
    const handleOpen = () => {
        if(title === "" || description === "" || status === ""){
            setInputMessage("You must pick and fill all the fields!")
        }else{
            setConfirm(true)
        }
    }

    // Handle status change
    const handleChange = (event) => {
        setStatus(event.target.value);
    };

    // Handle add group
    const onSubmit = async () => {
        setInputMessage("")

        // If status private is true
        if(status === true){
            const newGroup = {
                name: title,
                description: description,
                isPrivate: status,
                creator: user.id
            }

            const response = await createNewGroup(user.id, newGroup)

            if(response.status === 200){
                setSeverity("success")
                setOpen(true)
            }

            if(response.status !== 200){
                setSeverity("error")
                setOpen(true)
            }
        }
        // If status private is not true
        if(status === false){
            const newGroup = {
                name: title,
                description: description,
                isPrivate: status,
                creator: user.id
            }

            const response = await createNewGroup(user.id, newGroup)

            if(response.status === 200){
                setSeverity("success")
                setOpen(true)
            }

            if(response.status !== 200){
                setSeverity("error")
                setOpen(true)
            }
        }

        handleConfirmClose();
        
    }

    return(
        <div>
            {/* Add group*/}
            <Typography variant="h4" sx={{marginTop: 2}}>Create a new group</Typography>
            <Typography sx={{color: 'red', marginBottom: 2, marginTop: 1}}>{inputMessage}</Typography>
            <Box sx={{
                        display: 'flex',
                        alignItems: 'center',
                        flexDirection: 'column',
                        marginTop: 2,
                        marginBottom: 20,
                        '& > :not(style)': { m: 1 },
                }}>
                <div>
                    <TextField label="Name"  sx={{width: 400}} onChange={(e) => setTitle(e.target.value)}/> 
                    <Tooltip title="The name of your group, make it stick out. It can later be changed!" placement="bottom">
                        <QuestionMarkIcon sx={{color: 'GrayText', marginTop: 2, marginLeft: 1, borderStyle: 'solid', backgroundColor: 'whitesmoke', borderColor: 'whitesmoke', borderRadius: 10}}/>
                    </Tooltip>
                </div>
                <div>
                    <TextField label="Description" sx={{width: 400}} onChange={(e) => setDescription(e.target.value)} multiline rows={4}/>
                    <Tooltip title="The description of your group, whats is it about? It can later be changed!" placement="bottom">
                        <QuestionMarkIcon sx={{color: 'GrayText', marginTop: 2, marginLeft: 1, borderStyle: 'solid', backgroundColor: 'whitesmoke', borderColor: 'whitesmoke', borderRadius: 10}}/>
                    </Tooltip>
                </div>
                <Box >
                    <FormControl sx={{ minWidth: 400 }}>
                        <InputLabel id="demo-simple-select-label">Publicity status</InputLabel>
                        <Select
                            label="Publicity status"
                            id="select"
                            value={status}
                            onChange={handleChange}
                        >
                        <MenuItem value={true}>Private</MenuItem>
                        <MenuItem value={false}>Not private</MenuItem>
                    </Select>
                    </FormControl>
                    <Tooltip title="Choose the publicity status. If private only you can invite others and if not, anyone can join!" placement="bottom">
                        <QuestionMarkIcon sx={{color: 'GrayText', marginTop: 1.7, marginLeft: 1, borderStyle: 'solid', backgroundColor: 'whitesmoke', borderColor: 'whitesmoke', borderRadius: 10}}/>
                    </Tooltip>
                </Box>
                <Button variant="contained" onClick={handleOpen}>Create group</Button>
            </Box>
            
            {/* Alert*/}
            {severity === "error" ? 
                <Snackbar open={open} autoHideDuration={5000} onClose={handleClose}>
                    <Alert severity="error">Could not create the group!</Alert>
                </Snackbar> : 
                <Snackbar open={open} autoHideDuration={5000} onClose={handleClose}>
                    <Alert severity="success">Group created successfully!</Alert>
                </Snackbar>
            }

            {/* Dialog conformation*/}
            <Dialog
                open={confirm}
                onClose={handleOpen}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
                >
                <DialogTitle id="alert-dialog-title">
                {"Confirmation"}
                </DialogTitle>
                <DialogContent>
                <DialogContentText id="alert-dialog-description">
                    Do you want to create the group: {title}?
                </DialogContentText>
                </DialogContent>
                <DialogActions>
                <Button onClick={handleConfirmClose}>No</Button>
                <Button onClick={() => onSubmit()}>
                    Yes
                </Button>
                </DialogActions>
            </Dialog>
        </div>
    )
}

export default withAuth(AddGroup);
import { useState, useEffect } from "react";
import { createTopicAndAssignCreatorAsSubscriber, getAllTopics } from "../../api/topicEndpoints";
import TopicsComponent from "../../components/Topics/TopicsComponent";
import usePagination from "../../utils/Pagination";
import { Button, Dialog, DialogActions, DialogContent, DialogTitle, Pagination, TextField, Card } from "@mui/material";
import { useUser } from "../../context/UserContext";

const Topics = () => {
    
    //State declarations
    const [stateTopics, setStateTopics] = useState([])
    const [topicName, setTopicName] = useState("");
    const [topicDescription, setTopicDescription] = useState("");
    const [openCreateTopic, setOpenCreateTopic] = useState(false)
    const [valid, setValid] = useState(false);
    const { user } = useUser();  
    let [topicSearchValue, setTopicSearchValue] = useState("");

    // Pagination
    let [topicPage, setTopicPage] = useState(1);
    const TOPICS_PER_PAGE = 2;
    const topics_count = Math.ceil(stateTopics.length / TOPICS_PER_PAGE);
    const TOPIC_DATA = usePagination(stateTopics, TOPICS_PER_PAGE);

    //Topic page change
    const handleTopicChange = (e, p) => {
        setTopicPage(p);
        TOPIC_DATA.jump(p);
    }


    useEffect(() => {
        let abortController = new AbortController();
        //Fetch topics 
        const fetchData = async () => {
            const response = await getAllTopics();
            setStateTopics(response.data)
            
        }
        fetchData();

        return () => {  

            abortController.abort();  

        }
    }, [stateTopics.some(x => x)])

    //Creates topic, http post
    const createTopic = async () => {
        const topicData = {
            name: topicName,
            description: topicDescription,
            creator: user.id
        }

        await createTopicAndAssignCreatorAsSubscriber(user.id, topicData)
        window.location.reload()
    }

    //Opens create topic modal
   const handleOpenCreateTopic = () => {
        setOpenCreateTopic(true);
    }

    //Closes create topic modal
    const handleOpenCreateTopicClose = () => {
        setOpenCreateTopic(false);
    }
    return(
        <>
        {/* Search bar and button */}
        <div style={{display:"flex", flexDirection: "row", width: "100vw", justifyContent: "center", margin: "0 auto"}}>
            <TextField label="Search Topics.." variant="outlined" style={{width: 600, marginBottom: 25, marginTop: 25}} onChange={(event) => {setTopicSearchValue(event.target.value)}}/>
            <Button style={{width: "15vw", marginBottom: 25, marginTop: 25}} variant="outlined" onClick={handleOpenCreateTopic}>Create topic</Button>
        </div>
        {/* Pagination data filter */}
            { TOPIC_DATA.currentData().filter((topic) => {
                if(topicSearchValue === ''){
                    return  <>
                                {<TopicsComponent topics={topic} key={topic.id}/> }
                            </>
                    } else if(topic.name.toLowerCase().includes(topicSearchValue.toLowerCase())){
                    return  <>
                                {<TopicsComponent topics={topic} key={topic.id}/> }
                            </>
                    }
                    return null
            })
            .map((topic , i) => (
                <TopicsComponent topics={topic} key={topic.id}/>
            ))}

            {/* Pagination */}
            <div style={{borderTop:"1px solid #E7E7E7", marginTop: 25}}>
                <Pagination
                count={topics_count}
                size="large"
                page={topicPage}
                variant="outlined"
                shape="rounded"
                onChange={handleTopicChange}
                style={{display: "flex", flexDirection: "row", justifyContent: "center", marginTop: 10, marginBottom: 10}}
                />
            </div>

            <div>
                {/* Modal for create topic */}
                    <Dialog
                        open={openCreateTopic}
                        // onClose={handleEventClickClose}
                        aria-describedby="alert-dialog-slide-description"
                        maxWidth="md"
                        >
                            <DialogTitle>
                                Create a new Topic
                            </DialogTitle>
                            <DialogContent >
                                <Card elevation={3} 
                                style={{
                                    display:"flex", 
                                    flexDirection:"row", 
                                    justifyContent:"space-between",
                                    padding: "20",
                                    minWidth: "30vw"}}>
                                    <div style={{marginBottom: "2vh", display: "flex", flexDirection:"column", margin: "0 auto", padding: 20}}>
                                        <TextField style={{marginRight: "1vw", marginBottom: 10, width: "41vw"}} label="Topic title" variant="outlined" onChange={(e) => setTopicName(e.target.value)}
                                        error={topicName === "" ? !valid : valid}
                                        required={true}/>

                                        <TextField style={{marginRight: "1vw", marginBottom: 10}} 
                                        label="Topic description" variant="outlined" 
                                        multiline rows={4} 
                                        onChange={(e) => setTopicDescription(e.target.value)}
                                        error={topicDescription === "" ? !valid : valid}
                                        required={true}/>
                                        
                                        {topicDescription !== ""  && topicName !== "" ? <Button type="submit" onClick={() => createTopic()}>Create new Topic</Button> : null}
                                    </div>
                                </Card>
                            </DialogContent>
                            <DialogActions>
                                <Button onClick={handleOpenCreateTopicClose}>Close</Button>
                            </DialogActions>
                    </Dialog>
                </div>
        </>
    )
}

export default Topics;
// Validate the storage key
const validateKey = key => {
    if(!key || typeof key !== 'string'){
        throw new Error('storageSave: Invalid storage key was provided')
    }
}

// Save to storage
export const storageSave = (key, value) => {
    validateKey(key)

    if(!value){
        throw new Error('storageSave: No value provided for ' + key )
    }
    localStorage.setItem(key, JSON.stringify(value))
}

// Read the storage
export const storageRead = key => {
    validateKey(key)
    
    const data = localStorage.getItem(key);
    if(data){
        return JSON.parse(data)
    }

    return null;
}

// Delete the localStorage
export const storageDelete = key => {
    validateKey(key)
    localStorage.removeItem(key)
}

// Check the storage
export const storageCheck = key => {
    const data = localStorage.getItem(key)
    const jsonData = JSON.parse(data)
    if(jsonData.translations.length === 0){
        return true
    }else{
        return false;
    }
}

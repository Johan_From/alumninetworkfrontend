import { Card } from "@mui/material";
import { useEffect, useState } from "react";
import moment from 'moment'

    
    const TimelineEvent = ({events}) => {

    //State declarations
    const [eventId, setEventId] = useState("");
    const [eventName, setEventName] = useState("");
    const [eventDescription, setEventDescription] = useState("");
    const [eventStarTime, setEventStartTime] = useState("");
    const [eventEndTime, setEventEndTime] = useState("");
    const [eventAllowedGuests, setEventAllowedGuests] = useState("");
    const [eventInvitedGuests, setEventInvitedGuests] = useState("");
    
    useEffect(() => {
        let abortController = new AbortController();
        //Set state with destructured events
        setEventId(events.id);
        setEventName(events.name);
        setEventDescription(events.description)
        setEventStartTime(events.startTime)
        setEventEndTime(events.endTime)
        setEventAllowedGuests(events.allowedGuests)
        setEventInvitedGuests(events.invitedGuests)

        return () => {  

            abortController.abort();  

        } 
    }, [])



    return (
        <>
                        {/* Card for event */}
            <Card elevation={3} style={{marginTop: 10, marginBottom: 10}}>
                <div key={events.id}>
                    <div style={{borderBottom: "1px solid #E7E7E7"}}>
                        <h1 style={{ width: "60%", margin: "0 auto"}}>{events.name}</h1>
                    </div>
                    <p>{events.description}</p>
                    <div style={{
                    display:"flex", 
                    flexDirection: "row", 
                    justifyContent:"space-between",
                    fontSize: 10,
                    borderTop: "1px solid #E7E7E7"
                    }}>
                        <p style={{marginLeft: 10}}>Allowed guests {events.allowedGuests}</p>
                        <p style={{marginRight: 10}}>Invited guests {events.invitedGuests}</p>
                    </div>
                    <div style={{
                    display:"flex", 
                    flexDirection: "row", 
                    justifyContent:"space-between",
                    fontSize: 10
                    }}>
                        <p style={{marginLeft: 10}}>Started {moment(events.startTime).format('yyyy/MM/DD')}</p>
                        <p style={{marginRight: 10}}>Ending {moment(events.endTime).format('yyyy/MM/DD')}</p>
                    </div>
                </div>
            </Card>
        </>
    )
}

export default TimelineEvent;
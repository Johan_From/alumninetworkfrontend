import { useEffect, useState } from "react";
import { getPostRepliesById } from "../../api/postEndpoints";
import Card from '@mui/material/Card';
import Button from '@mui/material/Button';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import Typography from '@mui/material/Typography';
import CardContent from '@mui/material/CardContent';
import { getSpecificUser } from "../../api/userEndpoints";
import { useUser } from '../../context/UserContext'
import moment from 'moment'


const TimelinePost = ({posts, groups}) => {

    // State declarations
    const [postId, setPostId] = useState("");
    const [eventId, setEventId] = useState("");
    const [groupId, setGroupId] = useState("");
    const [topicId, setTopicId] = useState("");
    const [senderId, setSenderId] = useState("");
    const [postTitle, setPostTitle] = useState("");
    const [postDescription, setPostDescription] = useState("");
    const [postReplies, setPostReplies] = useState([]);
    const [postTimestamp, setPostTimestamp] = useState("")
    const [show, setShow] = useState(false);
    const [replies, setReplies] = useState([])
    const [users, setUsers] = useState([])
    const [postGroup, setPostGroup] = useState([]);
    const [replyDescription, setReplyDescription] = useState("")

    //Empty array which is used to store replies in useEffect
    let tempArray = [];
    
    

    useEffect(() => {
        let abortController = new AbortController();
        //Set state with destructured posts and groups
        setPostId(posts.id);
        setEventId(posts.eventId);
        setGroupId(posts.groupId);
        setTopicId(posts.topicId);
        setSenderId(posts.topicId);
        setPostTitle(posts.title);
        setPostDescription(posts.description);
        setPostReplies(posts.postReplies);
        setPostTimestamp(posts.timeStamp)
        setPostGroup(groups)
        setReplyDescription(postReplies.description)


        //Fetch replies and add users to tempArray
        const fetchData = async () => {
           postReplies.forEach(async id => {
                let response = await getPostRepliesById(id);
                tempArray.push(response.data)
                const composed = tempArray.map(x => x.map(d => {
                    return {
                        ...d,
                        users: users
                            
                        }
                        
                    }));
                    setReplies(composed);
            });

                
        } 
        fetchData();
        
        return () => {  

            abortController.abort();  

        }  
            
        }, [posts.description, posts.eventId, posts.groupId, posts.id, posts.postReplies, posts.timeStamp, posts.title, posts.topicId, show])

        //Fetches users 
        const fetchSpecificUserData = async () => {
            // Array of user sender id from replies
            const arrayOfSenderIds = replies.map(array => array.map(postReply => postReply.senderId))            
            let arrayOfIds = []
            arrayOfSenderIds.map(id => id).map(x => x.map(u =>{
                arrayOfIds.push(u)
            }));
            
            let tempUsersArray = [];
            //Loops through array of sender ids and makes api calls to fetch users
            arrayOfIds.forEach(async (id) => {
                const response = await getSpecificUser(id);
                tempUsersArray.push(response)

                const composed = replies.map(x => x.map(d => {
                    return {
                      ...d,
                      users: tempUsersArray.filter(({id}) => d.senderId === id).map(x => x).map(x => x.name).filter((v, i, a) => a.indexOf(v) === i)
                    }
                }));

                var usersArray = composed.map(x => x.map(x => x.users)) 
                setUsers(usersArray)
                setReplies(composed);
                
            })
            setShow(!show)
        }

    return (
        

            <div key={posts.id} id={posts.id}>
                                    {/* Card for displaying post */}
                <Card style={{marginBottom: 10, marginTop: 10, minHeight: 230, backgroundColor: "#F8F8F8"}} elevation={3} sx={{minWidth: 600}}>
                    <Card style={{}} elevation={0} sx={{minWidth: 600, minHeight: 190}} className="timeline-post-card-wrapper">
                        <CardContent>
                            <Typography gutterBottom variant="h5" className="timeline-post-card-header">
                                <p style={{
                                    fontSize: 25, 
                                    position: "absolute", 
                                    left: 0, margin:0, 
                                    marginLeft: "1vw", 
                                    display:"inline-block"}}
                                    >{postTitle}</p>
                                <div style={{display: "flex", flexDirection:"column"}}>
                                    <p style={{
                                    fontSize: 10, 
                                    position:"absolute", 
                                    right: 0, 
                                    display:"inline-block", 
                                    marginTop: "0.5vh"}}
                                    >{moment(postTimestamp).format('yyyy/MM/DD - hh:mma')}</p>

                                {postGroup.length > 0 ?  
                                    <p style={{
                                    fontSize: 10, 
                                    position:"absolute", 
                                    right: 0, 
                                    display:"inline-block", 
                                    marginTop: "3vh"}}
                                    >Target Group: <i style={{fontWeight:"bold"}}>{postGroup}</i> </p> : null}
                                </div>
                            </Typography>
                        </CardContent>
                        <Accordion style={{marginTop: 20 }}>
                            <AccordionSummary
                            expandIcon={<ExpandMoreIcon />}
                            aria-controls="panel1a-content"
                            id="panel1a-header">
                            <h4>Description</h4>
                            </AccordionSummary>
                            <AccordionDetails>
                            <Typography>
                                {postDescription}
                            </Typography>
                            </AccordionDetails>
                        </Accordion>
                        {postReplies.length === 0 ? null : <Button size="small" onClick={() => fetchSpecificUserData()}>{show === false ? <p>Show replies</p> : <p>Hide replies</p>}</Button>}
                    </Card>
                {show ? 
                <div>
                    {/* Prints out replies which shows on click of a button */}
                    {replies.map((postReply) => (
                        postReply.map((reply, i) => ( 
                        
                        <Card style={{ width: 600, margin: "0 auto", marginBottom: 15}} key={i} id={i}>
                            <div style={{display:"flex", flexDirection: "row", justifyContent: "space-between", }}>
                                <h5 style={{marginLeft: "1vw"}}>{reply.title}</h5>
                                <p style={{fontSize: 10, marginRight: "1vw"}}>{reply.timeStamp}</p>
                            </div>
                            <div style={{border:"2px solid #F8F8F8"}}>
                                <p>{reply.description}</p>
                            </div>
                            <div style={{position:"relative"}}>
                                {/* {reply.users.map((x, i) => (
                                    <></>// <p key={i} style={{ float: "right", fontSize: 10, marginRight: 10}}>Posted by {x}</p>
                                ))} */}
                            </div>
                        </Card>
                        
                        ))
                    ))}
                </div> : null}
                </Card>

            </div>

        
    )
}

export default TimelinePost;
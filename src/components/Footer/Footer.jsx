import { Box, Typography } from "@mui/material";
import { useNavigate } from "react-router-dom";

const Footer = () => {
    const navigate = useNavigate();
    var phantom = {
        display: 'block',
        padding: '20px',
        height: '50px',
        borderTop: "1px solid #E7E7E7",
    }


    // Methods for redirecting to differnt pages and websites
    const redirectToAbout = () => {
        navigate("/about")
    }

    const openNewTabExperis = () => {
        window.open('https://www.experis.se/sv/it-tjanster/experis-academy', '_blank');
    }

    const openNewTabNoroff = () => {
        window.open('https://www.noroff.no/en/accelerate', '_blank');
    }

    return (
        <div style={phantom}>
            <div/>
            <footer>
                <Box sx={{width: '50%', float: "left",  marginBottom: 9}}>
                    <Box sx={{display: 'flex', flexDirection: 'column'}}>
                        <Typography sx={{color: 'white'}}>AlumniNetwork • 2022</Typography>
                        <Typography sx={{color: 'white', ":hover":{cursor: 'pointer', color: 'whitesmoke', textDecoration: 'underline'}}} onClick={redirectToAbout}>About</Typography>
                    </Box>
                </Box>
                <Box sx={{width: '50%', float: "right",  marginBottom: 9}}>
                    <Box id="icons" sx={{display: 'flex', flexDirection: 'column'}}>
                        <Typography sx={{color: 'white', ":hover":{textDecoration: 'underline', color: 'whitesmoke', cursor: 'pointer'}}} onClick={openNewTabExperis}>Experis Academy</Typography>
                        <Typography sx={{color: 'white', ":hover":{textDecoration: 'underline', color: 'whitesmoke', cursor: 'pointer'}}} onClick={openNewTabNoroff}>Noroff Accelerate</Typography> 
                    </Box>
                </Box>
            </footer>
        </div>
        
    )
}

export default Footer;
import { useState, useEffect, forwardRef } from "react";
import { Accordion, AccordionDetails, AccordionSummary, Button, Card, CardContent, Dialog, DialogActions, DialogContent, DialogTitle, Snackbar, TextField, Typography } from "@mui/material";
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { createPost, getTopicPostsBasedOnTopicId, updatePost } from "../../api/postEndpoints";
import usePagination from "../../utils/Pagination";
import { Pagination } from "@mui/material";
import { useUser } from "../../context/UserContext";
import { getEventByEventId } from "../../api/eventEndpoints";
import FullCalendar from '@fullcalendar/react'
import dayGridPlugin from '@fullcalendar/daygrid'
import interactionPlugin from '@fullcalendar/interaction'
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';
import moment from 'moment'
import { assignUserToTopicByUserId } from "../../api/topicEndpoints";
import { getSpecificUser } from "../../api/userEndpoints";
import MuiAlert from '@mui/material/Alert';

const TopicsComponent = ({topics}) => {
    
    // State declarations
    const [topicName, setTopicName] = useState("");
    const [topicDescription, setTopicDescription] = useState("");
    const [stateTopic, setStateTopics] = useState([]);
    const [topicId, setTopicId] = useState([]);
    const [topicPosts, setTopicPosts] = useState([]);
    const [show, setShow] = useState([]); 
    const [topicEvents, setTopicEvents] = useState([]);
    const [open, setOpen] = useState(false);
    const [openEvent, setOpenEvent] = useState(false)
    const [stateCalendarEvents, setCalendarEvents] = useState({event: {title: "", start: new Date(), end: new Date()}});
    const { user } = useUser();  
    const [topicUsers, setTopicUsers] = useState([]); 
    const [postDescription, setPostDescription] = useState("");
    const [postTitle, setPostTitle] = useState("");
    const [openReplyPost, setOpenReplyPost] = useState(false);
    const [valid, setValid] = useState(false);
    const [editPostTitle, setEditPostTitle] = useState("");
    const [editPostDescription, setEditPostDescription] = useState("");
    const [openEditDialog, setOpenEditDialog] = useState(false);
    const [severity, setSeverity] = useState("")
    const [openSnackBar, setOpenSnackBar] = useState(false)

    //Pagination of topic replies 
    let [topicReplyPage, setTopicReplyPage] = useState(1);
    const TOPICS_REPLY_PER_PAGE = 2;
    //let arrayOfPostReplies = stateTopic.map(x => x.posts).flat();
    const topics_reply_count = Math.ceil(stateTopic.map(x => x.posts.length) / TOPICS_REPLY_PER_PAGE);
    const TOPIC_REPLY_DATA = usePagination(topicPosts, TOPICS_REPLY_PER_PAGE);

    // Page switch for topic replies 
    const handleTopicReplyChange = (e, p) => {
        setTopicReplyPage(p);
        TOPIC_REPLY_DATA.jump(p);

    }
    
    //Temp arrays
    let topicArray = [];
    let topicPostsArray = [];
    
    
    useEffect(() => {
        let abortController = new AbortController();
        //Set state
        setTopicName(topics.name);
        setTopicDescription(topics.description)
        topicArray.push(topics)

        //Fetch data from api
        const fetchData = async () => {
            fetchPostsBasedOnTopicId();
            fetchTopicUser();
        }
        fetchData();
        
        return () => {  

            abortController.abort();  

        } 

    }, [topicPosts.some(x => x)]) 
    
    //Fetches posts for each topic
    const fetchPostsBasedOnTopicId = async () => {
        const replyUsers = []
        
        //Loops through array of topics and fetches topics
        topicArray.forEach(async (x) => {
            const response = await getTopicPostsBasedOnTopicId(x.id)
            topicPostsArray.push(response.data)
            setTopicPosts(topicPostsArray.flat())
            
            
            //Gets sender from topic posts and loops trough senders and gets users
            const sender = topicPostsArray.flat().map(x => x.senderId);
            sender.forEach(async (x) => {
                const response = await getSpecificUser(x)
                replyUsers.push(response)
                const composed = topicPostsArray.flat().map(d => {
                    return {
                        ...d,
                        users: replyUsers.filter(({id}) => d.senderId === id).map(x => x.name).filter((v, i, a) => a.indexOf(v) === i)
                    }
                })
                setTopicPosts(composed);
            })
        })
    
                
    
    }

    //Gets topic user
    const fetchTopicUser = async () => {

        //Topic creator id
        const creator = topics.creator;

        //Fetches user with creator
        const response = await getSpecificUser(creator);

        //Sets user for topics
        setTopicUsers(response);

        //Adds posts and topics to array and sets state with new array
        const composed = topicArray.map(d => {
            return {
                ...d,
                posts: topicPosts.filter(({topicId}) => d.id === topicId),
                users: topicUsers
            }
        })
        setStateTopics(composed);
    }

    //Toggles between false and true
    const toggleShow = () => {
        setShow(!show);
    }

    //Gets all events on click and sets events for topic
    const handleClickOpen = async () => {
        let topicEventArray = []
        topics.events.forEach( async (eventId) => {
            const eventResponse = await getEventByEventId(eventId);
            topicEventArray.push(eventResponse.data)
            setTopicEvents(topicEventArray.flat());
            
        }) 
        setOpen(true);
    };
    
    //On close, set open to false
    const handleClose = () => {
        setOpen(false);
        
    };

    //Events for calendar
    const calendarEvents = () => {
        let events = topicEvents.map(event => (
            {
                title: event.name, 
                start: moment(event.startTime).toISOString(), 
                end: moment(event.endTime).toISOString(),
            }
        ))
        return events;
    }


    //On clicking on event in calendar opens event details
    const handleEventClickOpen = ({event}) => {
        setOpenEvent(true);
        setCalendarEvents({event});
    }
  
    //On click close event dialog
    const handleEventClickClose = () => {
      setOpenEvent(false);
    }

    //Adds user to topic on click
    const joinTopic = async (topicId, userId) => {
        const response = await assignUserToTopicByUserId(topicId, userId);
        if(response.status === 200){
            setSeverity("success")
            setOpenSnackBar(true)
        }
        if(response.status !== 200){
            setSeverity("error")
            setOpenSnackBar(true)
        }
        window.location.reload();

    }

    //User feedback on close
    const handleSnackBarClose = (event, reason) => {
        if (reason === 'clickaway') {
          return;
        }
    
        setOpenSnackBar(false);
    };

    //Creates post in topic
    const createPostInTopic = async () => {

        const currentDate = new Date().toISOString()
        const postData = {
            description: postDescription,
            title: postTitle,
            timeStamp: currentDate,
            senderId: user.id,
            topicId: topics.id,
            userId: 0,
            groupId: 0,
            eventId: 0,
            replyId: 0

        }
        await createPost(postData)
        await fetchPostsBasedOnTopicId()
        window.location.reload()
    }

    //Opens replies in posts
    const handleOpenReplyPost = () => {
        setOpenReplyPost(true);
        
    }

    //Closes replies in post
    const handleOpenReplyPostClose = () => {
        setOpenReplyPost(false);
        setPostDescription("");
        setPostTitle("");
    }

    //Opens specific edit post 
    const handleOpenEditPost = (id) => {
        setOpenEditDialog({
            ...openEditDialog,
            [id]: !openEditDialog[id],

        });
        
    }

    //Closes edit post dialog
    const handleOpenEditPostClose = () => {
        setOpenEditDialog(false);
    }

    // Updates post on click
    const handleUpdate = async (postId) => {
            const currentDate = new Date().toISOString();

            const putData = {
                id: postId,
                timeStamp: currentDate,
                title: editPostTitle,
                description: editPostDescription
            }

            await updatePost(putData)
            window.location.reload()
        
    }

    //Alert for user feedback
    const Alert = forwardRef(function Alert(props, ref) {
        return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
    });


    return(
        <>
            <div key={topicId} style={{marginBottom: 25}}>
                    {/* Card for Topic */}
                <Card style={{marginBottom: 25, marginTop: 10, minHeight: 230, backgroundColor: "#F8F8F8", width: 800, margin: "0 auto", marginBottom: 25}} elevation={3} sx={{minWidth: 1000}}>
                <Card style={{}} elevation={0} sx={{minWidth: 1000, minHeight: 190}} className="timeline-post-card-wrapper">
                    <CardContent>
                        <Typography gutterBottom variant="h5" className="timeline-post-card-header">
                            <p style={{
                                fontSize: 25, 
                                position: "absolute", 
                                left: 0, margin:0, 
                                marginLeft: "1vw", 
                                display:"inline-block"}}
                                >{topicName}</p>
                            <div style={{display: "flex", flexDirection:"column"}}>
                                <p style={{
                                fontSize: 10, 
                                position:"absolute", 
                                right: 0, 
                                display:"inline-block", 
                                marginTop: "0.5vh"}}
                                >{}</p>

                            <Button onClick={() => handleClickOpen()} style={{
                                fontSize: 10, 
                                position:"absolute", 
                                right: 0, 
                                display:"inline-block", 
                                }} variant="outlined">
                                <CalendarMonthIcon/>
                            </Button>

                            {topics.users.includes(user.id) ? <Button style={{
                                    fontSize: 10, 
                                    position:"absolute", 
                                    right: 0,
                                    marginRight: 70, 
                                    display:"inline-block",
                                    color: "green"
                                    }} disabled variant="outlined">
                                    You are a Member of this Topic
                                </Button>
                                : 
                                <Button onClick={() => joinTopic(topics.id, user.id)} style={{
                                    fontSize: 10, 
                                    position:"absolute", 
                                    right: 0,
                                    marginRight: 70, 
                                    display:"inline-block", 
                                    }} variant="outlined">
                                    Join Topic
                                </Button>
                            }
                            
   
                            <p style={{
                                fontSize: 10, 
                                position:"absolute", 
                                right: 0,
                                marginRight: 70, 
                                display:"inline-block",
                                marginTop: 30, 
                                }}>Topic created by {topicUsers.name}</p>
                        
  
                            </div>
                        </Typography>
                    </CardContent>
                    <Accordion style={{marginTop: 20 }}>
                        <AccordionSummary
                        expandIcon={<ExpandMoreIcon />}
                        aria-controls="panel1a-content"
                        id="panel1a-header">
                        <h4>Description</h4>
                        </AccordionSummary>
                        <AccordionDetails>
                        <Typography>
                            {topicDescription}
                        </Typography>
                        </AccordionDetails>
                    </Accordion>
                    <div style={{display:"flex", flexDirection: "column"}}>
                       {topics.users.includes(user.id) ? <Button onClick={handleOpenReplyPost} style={{fontSize: 10}}>Create post in Topic</Button>: null }
                        {topicPosts.length > 0 ? <Button onClick={toggleShow}> {show ?  <p>Show posts in Topic</p> : <p>Hide posts</p>}</Button> : null}
                    </div>

                </Card>
                {!show ? 
                <div>
                    {/* Prints out replies in topic */}
                    {TOPIC_REPLY_DATA.currentData().map((topicPost, i) => (
                        
                        <>
                            <Card style={{ width: 600, margin: "0 auto", marginBottom: 15}} key={topicPost.id}>
                                    <div style={{display:"flex", flexDirection: "row", justifyContent: "space-between", }}>
                                        <h5 style={{marginLeft: "1vw"}}>{topicPost.title}</h5>
                                        <p style={{fontSize: 10, marginRight: "1vw"}}>{topicPost.timeStamp}</p>
                                        
                                    </div>
                                    <div style={{border:"2px solid #F8F8F8"}}>
                                        <p>{topicPost.description}</p>
                                    </div>
                                    <div style={{position:"relative"}}>
                                            {topicPost.senderId === user.id ? <Button onClick={() => handleOpenEditPost(topicPost.id)}> { !openEditDialog[topicPost.id] ? <p style={{fontSize: 10}}>Edit post</p> : <p style={{fontSize: 10}}>Close Edit</p> }</Button> : null}
                                    </div>
                                    
                                    <div>
                                        <Card elevation={1} 
                                            style={{
                                                display:"flex", 
                                                flexDirection:"row", 
                                                justifyContent:"space-between",
                                                padding: "20",
                                                minWidth: "20vw",
                                                display: openEditDialog[topicPost.id] ? 'block' : 'none'}}
                                                >
                                            <div style={{marginBottom: "2vh", display: "flex", flexDirection:"column", margin: "0 auto", padding: 20}}>
                                                <TextField style={{marginRight: "1vw", marginBottom: 10}} label="Post title" variant="outlined" value={editPostTitle} onChange={(e) => setEditPostTitle(e.target.value)}
                                                error={editPostTitle === "" ? !valid : valid}
                                                required={true}/>
            
                                                <TextField style={{marginRight: "1vw", marginBottom: 10}} 
                                                label="Post description" variant="outlined" 
                                                multiline rows={4} 
                                                value={editPostDescription}
                                                onChange={(e) => setEditPostDescription(e.target.value)}
                                                error={editPostDescription === "" ? !valid : valid}
                                                required={true}/>
                                                
                                                {editPostDescription !== ""  && editPostTitle !== "" ? <Button  onClick={() => handleUpdate(topicPost.id)}>Edit</Button> : null}
                                            </div>
                                            </Card>
                                    
                                    </div>
                                    <div>
                                        <p style={{float: "right", marginRight: "2vw", fontSize: 10}}>Posted by {topicPost.users}</p>
                                    </div>
                                        
                                    
                            </Card>
                            
                        </>
               
                        ))
                    }
                    {/* Pagination for topics */}
                    <div>
                        <Pagination
                        count={topics_reply_count}
                        size="large"
                        page={topicReplyPage}
                        variant="outlined"
                        shape="rounded"
                        onChange={handleTopicReplyChange}
                        style={{display: "flex", flexDirection: "row", justifyContent: "center", marginBottom: 25}}
                        />
                    </div>
                </div> : null }
                
                {/* Calendar modal */}
            </Card>
                <Dialog
                open={open}
                onClose={handleClose}
                aria-describedby="alert-dialog-slide-description"
                fullWidth
                maxWidth="lg"
                style={{backgroundColor: "#1976d2"}}
                >
                <DialogTitle>{"Check out your calendar"}</DialogTitle>
                    <DialogContent>
                        <FullCalendar
                        plugins={[ dayGridPlugin, interactionPlugin ]}
                        initialView="dayGridMonth"
                        events={calendarEvents()}
                        eventClick={handleEventClickOpen}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleClose}>Close</Button>
                    </DialogActions>
                </Dialog>
                <div>
                
            </div>
            </div>
            <div>   
                {/* Opens modal on event click */}
                    <Dialog
                        open={openEvent}
                        // onClose={handleEventClickClose}
                        aria-describedby="alert-dialog-slide-description"
                        maxWidth="md"
                        >
                            <DialogTitle>
                                {stateCalendarEvents.event.title}
                            </DialogTitle>
                            <DialogContent >
                                <Card elevation={3} 
                                style={{
                                    display:"flex", 
                                    flexDirection:"row", 
                                    justifyContent:"space-between",
                                    padding: "20",
                                    minWidth: "60vw"}}>
                                    <p style={{marginLeft: "2vw"}}> <span style={{fontWeight: "bold"}}>Started at</span>  {moment(stateCalendarEvents.event.start).format('yyyy/MM/DD - H:mma')}</p>
                                    <p style={{marginRight: "2vw"}}> <span style={{fontWeight: "bold"}}>Ends at</span>  {moment(stateCalendarEvents.event.end).format('yyyy/MM/DD - H:mma ') }</p>
                                </Card>
                            </DialogContent>
                            <DialogActions>
                                <Button onClick={handleEventClickClose}>Close</Button>
                            </DialogActions>
                    </Dialog>
                </div>
                <div>
                    {/* Modal for create a post in topic */}
                    <Dialog
                        open={openReplyPost}
                        aria-describedby="alert-dialog-slide-description"
                        maxWidth="md"
                        >
                            <DialogTitle>
                                Create a new Post in {topicName}
                            </DialogTitle>
                            <DialogContent >
                                <Card elevation={3} 
                                style={{
                                    display:"flex", 
                                    flexDirection:"row", 
                                    justifyContent:"space-between",
                                    padding: "20",
                                    minWidth: "30vw"}}>
                                <form style={{marginBottom: "2vh", display: "flex", flexDirection:"column", margin: "0 auto", padding: 20}}>
                                    <TextField style={{marginRight: "1vw", marginBottom: 10, width: "41vw"}} label="Post title" variant="outlined" onChange={(e) => setPostTitle(e.target.value)}
                                    error={postTitle === "" ? !valid : valid}
                                    required={true}/>

                                    <TextField style={{marginRight: "1vw", marginBottom: 10}} 
                                    label="Post description" variant="outlined" 
                                    multiline rows={4} 
                                    onChange={(e) => setPostDescription(e.target.value)}
                                    error={postDescription === "" ? !valid : valid}
                                    required={true}/>
                                    
                                    {postDescription !== ""  && postTitle !== "" ? <Button type="submit" onClick={createPostInTopic}>Create Post</Button> : null}
                                </form>
                                </Card>
                            </DialogContent>
                            <DialogActions>
                                <Button onClick={handleOpenReplyPostClose}>Close</Button>
                            </DialogActions>
                    </Dialog>
                </div>
                {severity === "error" ? 
                    <Snackbar open={openSnackBar} autoHideDuration={5000} onClose={handleSnackBarClose}>
                        <Alert severity="error">Error: Could not subscribe to topic!</Alert>
                    </Snackbar> : 
                    <Snackbar open={openSnackBar} autoHideDuration={5000} onClose={handleSnackBarClose}>
                        <Alert severity="success">Success: Subscribed to topic!</Alert>
                    </Snackbar>
                }
        </>
    )
}

export default TopicsComponent;
import { Button, Card, Snackbar } from "@mui/material";
import moment from "moment";
import { forwardRef, useEffect, useState } from "react";
import { inviteGroupToEvent } from "../../api/eventEndpoints";
import { getAllGroupsWithUser } from "../../api/groupEndpoints";
import { useUser } from "../../context/UserContext";
import MuiAlert from '@mui/material/Alert';

const SpecificEvent = ({ event }) => {
    // States for getting users and groups.
    const [groups, setGroups] = useState([]);
    const {user} = useUser();
    const [groupId, setGroupId] = useState([]);
    const [open, setOpen] = useState(false)
    const [severity, setSeverity] = useState("")

    useEffect(() => {
        let abortController = new AbortController();
        // Fetches groups by user id
        const fetchData = async () => {
            const fetchGroups = await getAllGroupsWithUser(user.id);
            setGroups(fetchGroups.data);
        }
        fetchData();

        return () => {
            abortController.abort();
        }
    }, [])

    // Adds the event to the group specified by groupId
    const onSubmitGroup = async (eventId) => {
        const response = await inviteGroupToEvent(groupId, eventId)
        if(response.status === 200){
            setSeverity("success")
            setOpen(true)
        }

        if(response.status !== 200){
            setSeverity("error");
            setOpen(true)
        }


    }

    const Alert = forwardRef(function Alert(props, ref) {
        return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
    });

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
          return;
        }
    
        setOpen(false);
    };

    // Maps out the groups on the Event page
    const mapGroups = (event) => {
        
        return (<div>
            <select style={{ padding: 4, fontSize: 20, borderRadius: 6}} key={event.id} name="Select Group" value={groupId} onChange={(e) => setGroupId(e.target.value)} multiple={true}>
                {groups.map((group) =>{
                if(!group.events.includes(event.id)){
                    return (
                        <option style={{color: 'black'}} key={group.id} value={group.id}>{group.name}</option>
                        )
                    }
                })
                }
            </select>
            <br/>
            <Button sx={{marginTop: 1, marginBottom: 1}} variant="contained" onClick={() => onSubmitGroup(groupId, event.id)}>Invite Group</Button>
            </div>
        )
    }

    return(
        <>
            <Card elevation={3} sx={{marginBottom: 2}}>
                <h2>{event.name}</h2>
                <p>Invited guests: {event.invitedGuests}</p>
                <p>Allowed guests: {event.allowedGuests}</p>
                <p>Event Starts: {moment(event.startTime).format('yyyy/MM/DD - H:mma')}</p>
                <p>Event Ends: {moment(event.endTime).format('yyyy/MM/DD - H:mma')}</p>
                    {mapGroups(event)}
            </Card>
            {severity === "error" ? 
                    <Snackbar open={open} autoHideDuration={5000} onClose={handleClose}>
                        <Alert severity="error">Could not invite group to event!</Alert>
                    </Snackbar> : 
                    <Snackbar open={open} autoHideDuration={5000} onClose={handleClose}>
                        <Alert severity="success">Event invited successfully successfully, Refresh page to update!</Alert>
                    </Snackbar>
                }
            </>
        
    )
}

export default SpecificEvent;
import { Box, Accordion, AccordionDetails, AccordionSummary, Card, Typography } from "@mui/material";
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';

// Specific user component
const SpecificUser = ({ users }) => {
    // Capitalize first letter of each name
    const caitalizeFirstLetters = (string) => {
        return string
	        .split(" ")
	        .map(arr => arr.charAt(0).toUpperCase() + arr.slice(1))
	        .join(' ')
    }

    return(
        <Card elevation={3} sx={{ minWidth: 290, marginRight: 1.5, marginBottom: 1.5, marginLeft: 1.5}}>
              {/* Card for use infromation */}
            <Accordion style={{marginTop:0 }}>
                <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel1a-content"
                    id="panel1a-header"
                    sx={{display: 'flex', flexDirection: 'column'}}>
                    <Box>
                        <Typography variant="h5" component="div" sx={{borderBottom: "1px solid #E7E7E7"}}>
                            {caitalizeFirstLetters(users.name)}
                        </Typography>
                        <Typography sx={{ fontSize: 14 }} color="text.primary" gutterBottom>
                        {users.workStatus === "" ? "Unknown" : users.workStatus}
                        </Typography>
                    </Box>
                </AccordionSummary>
                
                <AccordionDetails>
                    <Card elevation={3} sx={{backgroundColor: 'whitesmoke', padding: 1.5}}>

                    
                    <Typography sx={{ fontSize: 14 }} color="text.primary" >
                        <strong>Biography: </strong> {users.bio === "" ? "Unknown" : users.bio}
                    </Typography>
                    <Typography sx={{ fontSize: 14 }} color="text.primary" >
                        <strong>Fun fact: </strong> {users.funFact === "" ? "Unknown" : users.funFact}
                    </Typography>
                    </Card>
                </AccordionDetails>
                
            </Accordion>
        </Card>
    )
}

export default SpecificUser;
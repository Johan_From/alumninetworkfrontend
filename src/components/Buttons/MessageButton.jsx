import MessageIcon from '@mui/icons-material/Message';
import { Tooltip } from '@mui/material';
import { useNavigate, useLocation } from 'react-router-dom';
import keycloak from '../../keycloak';

const MessageButton = () => {
    const navigate = useNavigate()
    const location = useLocation();

    const redirectSendMessage = () => {
        navigate("/directmessage")
    }

    const DisplayButton = () => {
        if(location.pathname !== "/directmessage"){
            return(
                <Tooltip title="Send message" placement="top">
                <MessageIcon 
                    onClick={redirectSendMessage}
                    sx={{
                    label: "Send message",
                    fontSize: 50, 
                    color: 'white', 
                    padding: 1.3, 
                    borderStyle: 'solid', 
                    borderRadius: 8.9,
                    backgroundColor: "#3884d1;",
                    ":hover":{
                        cursor: 'pointer',
                        backgroundColor: "#5a9bdc"
                    }
                    }}/>
            </Tooltip>
            )
        }
    }
    
    return(
        <div id="buttonDiv" >
            {keycloak.authenticated ?  <DisplayButton/> : null}  
        </div>
    )
}

export default MessageButton;
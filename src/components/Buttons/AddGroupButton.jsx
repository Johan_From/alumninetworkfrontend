import GroupAddIcon from '@mui/icons-material/GroupAdd';
import { Tooltip } from '@mui/material';
import { useNavigate, useLocation } from 'react-router-dom';
import keycloak from '../../keycloak';

const AddGroupButton = () => {
    const navigate = useNavigate()
    const location = useLocation();

    const redirectSendMessage = () => {
        navigate("/addgroup")
    }

    const DisplayButton = () => {
        if(location.pathname !== "/addgroup"){
            return(
                <Tooltip title="Add group" placement="top">
                <GroupAddIcon 
                    onClick={redirectSendMessage}
                    sx={{
                    label: "Send message",
                    fontSize: 50, 
                    color: 'white', 
                    padding: 1.3, 
                    borderStyle: 'solid', 
                    borderRadius: 8.9,
                    backgroundColor: "#3884d1;",
                    ":hover":{
                        cursor: 'pointer',
                        backgroundColor: "#5a9bdc"
                    }
                    }}/>
            </Tooltip>
            )
        }
    }
    
    return(
        <div id="addGroupDiv" >
            {keycloak.authenticated ?  <DisplayButton/> : null}  
        </div>
    )
}

export default AddGroupButton;
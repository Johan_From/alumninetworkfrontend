import { Accordion, AccordionDetails, AccordionSummary, Box, Button, Card, CardContent, Snackbar, TextField, Typography } from "@mui/material";
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import KeyboardControlKeyIcon from '@mui/icons-material/KeyboardControlKey';
import { forwardRef, useEffect, useState } from "react";
import MuiAlert from '@mui/material/Alert';
import { useUser } from "../../context/UserContext";
import { createPost, getPostRepliesById, updatePost } from "../../api/postEndpoints";
import { useParams } from "react-router-dom";
import { getSpecificUser } from "../../api/userEndpoints";
import moment from 'moment'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';

const SpecificGroupPost = ({ posts }) => {
    const {user} = useUser();
    const {id} = useParams();
    const [showReplies, setShowReplies] = useState(false);
    const [showReplyToPost, setShowReplyToPost] = useState(false)
    const [postReplies, setPostReplies] = useState([])

    const [title, setTitle] = useState("")
    const [description, setDescription] = useState("")
    const [inputMessage, setInputMessage] = useState("")
    const [severity, setSeverity] = useState("")
    const [open, setOpen] = useState(false)
    
    const [editShowPost, setEditShowPost] = useState(false)
    const [editTitle, setEditTitle] = useState("")
    const [editDescription, setEditDescription] = useState("")
    const [editInputMessage, setEditInputMessage] = useState("")
    const [openEdit, setOpenEdit] = useState(false)
    const [editSeverity, setEditSeverity] = useState("")

    const [senderName, setSenderName] = useState([])
    const [replyName, setReplyName] = useState([])

    const controller = posts.postReplies.length;

    useEffect(() => {
        // For useEffect
        let abortController = new AbortController();
        setEditTitle(posts.title)
        setEditDescription(posts.description)

        // empty arrays
        let repliesArr = []
        let replyUserArr = []

        const fetchData = async () => {
            posts.postReplies.forEach(async (replyId) => {
                const response = await getPostRepliesById(replyId) // get the post reply by id
                repliesArr.push(response.data) // add the data

                // Filter out the necessary 
                const composed = repliesArr.map(x => x.map(d => {
                    return {
                        ...d,
                        posts: repliesArr.filter(({id}) => d.replydId === id)
                    }
                }))

                setPostReplies(composed)

                // Get the reply name and status
                response.data.forEach(async (data) => {
                    const replyUser = await getSpecificUser(data.senderId)
                    replyUserArr.push(replyUser)

                    const removers = removeDuplicateObjectFromArray(replyUserArr, 'name')
                    setReplyName(removers)
                })
            })

            const usersReponse = await getSpecificUser(posts.senderId)
            setSenderName(usersReponse)            
        }

        fetchData()

        return () => {
            abortController.abort();
        }
    }, [controller, posts])

    // Helper method for filtering out duplicate objects
    const removeDuplicateObjectFromArray = (array, key) => {
        var check = new Set();
        return array.filter(obj => !check.has(obj[key]) && check.add(obj[key]));
    }

    // Alert method 
    const Alert = forwardRef(function Alert(props, ref) {
        return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
    });

    // Handle close for popup
    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
          return;
        }
    
        setOpen(false);
    };

    // Handle close for popup
    const handleEditClose = (event, reason) => {
        if(reason === 'clickaway'){
            return;
        }

        setOpenEdit(false)
    }

    // Method for replying to a post
    const replyToPost = async (postId) => {
        // Check if value is not empty
        if(title === "" || description === ""){
            setInputMessage("All the fields must contain text")
        }
        else{
            setInputMessage("")
            // Format the object
            const currentDate = new Date().toISOString();

            const replyData = {
                timeStamp: currentDate,
                title: title,
                description: description,
                senderId: user.id,
                userId: 0,
                groupId: id,
                eventId: 0,
                topicId: 0,
                replyId: postId
            }

            const response = await createPost(replyData);

            // Control the response, success och error
            if(response.status === 200){
                setSeverity("success")
                setOpen(true)
                setDescription("")
                setTitle("")
                setShowReplyToPost(false)
            }

            if(response.status !== 200){
                setSeverity("error")
                setOpen(true)
            }
        }
    }

    // Method for updating a post
    const handleUpdate = async (postId) => {
        // If empty
        if(editTitle === "" || editDescription === ""){
            setEditInputMessage("All the fields must contain a value!")
        }
        else{
            setEditInputMessage("")
            
            // Format object
            const currentDate = new Date().toISOString();

            const putData = {
                id: postId,
                timeStamp: currentDate,
                title: editTitle,
                description: editDescription
            }

            const response = await updatePost(putData)

            // Control response, success or error
            if(response.status === 204){
                setEditSeverity("success")
                setOpenEdit(true)
                setEditShowPost(false)
            }
            if(response.status !== 204){
                setEditSeverity("error")
                setOpenEdit(true)
            }
        }
    }

    return(
        <Card elevation={3} sx={{ minWidth: 275, marginRight: 1.5, marginBottom: 2, marginLeft: 1.5, backgroundColor: "#F8F8F8"}}>
            {/* Card for specific group post */}
            <Accordion >
                <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel1a-content"
                    id="panel1a-header"
                    sx={{display: 'flex', flexDirection: 'column'}}>
                    <Box>
                        <Typography variant="h5" component="div" sx={{borderBottom: "1px solid #E7E7E7"}}>
                            {posts.title}
                        </Typography>
                        <Typography sx={{ fontSize: 14 }} color="text.primary" gutterBottom>
                            <strong>Description:</strong> {posts.description}
                        </Typography>
                        <Typography sx={{ fontSize: 14 }} color="text.primary" gutterBottom>
                            <strong>Created:</strong> {moment(posts.timeStamp).format("yyyy/MM/DD - H:mma")}
                        </Typography>
                        <Typography sx={{ fontSize: 14 }}>
                            <strong>Created by:</strong> {senderName.name}
                        </Typography>
                            </Box>
                </AccordionSummary>
                
                <AccordionDetails>
                    {/* Button for showing replies */}
                    {postReplies.length >= 1 ?
                        <Button variant="contained" size="small" onClick={() => setShowReplies(!showReplies)}>{showReplies ? <>Hide replies <KeyboardControlKeyIcon sx={{marginLeft: 0.5}}/></> : <>Show replies<KeyboardArrowDownIcon sx={{marginLeft: 1}}/></>}</Button>
                     : null}
                    <Button variant="contained" size="small" sx={{marginLeft: 1}} onClick={() => setShowReplyToPost(!showReplyToPost)}>{showReplyToPost ? <>Close <KeyboardControlKeyIcon sx={{marginLeft: 0.5}}/></> : <>Reply<KeyboardArrowDownIcon sx={{marginLeft: 1}}/></>}</Button>
                
                    {posts.senderId === user.id 
                    ?<Button variant="contained" size="small" sx={{marginLeft: 1}} onClick={() => setEditShowPost(!editShowPost)}>{editShowPost ? <>Close <KeyboardControlKeyIcon sx={{marginLeft: 0.5}}/></> : <>Edit post<KeyboardArrowDownIcon sx={{marginLeft: 1}}/></>}</Button>
                     : null}
                    
                    
                    {showReplies ? 
                        <>
                        {/* Maps out the posts replies */}
                        {postReplies.map((postReply, i) => (
                            <div key={i}>
                            {posts.postReplies.length >= 1 ?
                                postReply.map((p, x) => (
                                    <div key={x}>
                                        <Card elevation={1} sx={{marginTop: 2, backgroundColor: 'whitesmoke'}} key={x}>
                                            <CardContent>
                                                <Typography sx={{ fontSize: 14 }}><strong>Title: </strong>{p.title}</Typography>
                                                <Typography sx={{ fontSize: 14 }}><strong>Description: </strong>{p.description}</Typography>
                                                <Typography sx={{ fontSize: 14 }}><strong>Last sent: </strong>{moment(p.timeStamp).format("yyyy/MM/DD - H:mma")}</Typography>
                                                {replyName.map((replier, y) => (
                                                    <div key={y}>
                                                        {p.senderId === replier.id ? <>{<Typography key={y} sx={{ fontSize: 14 }}><strong>Replied by: </strong> {replier.name}</Typography>}</>: null}
                                                        
                                                    </div>
                                                ))}
                                            </CardContent>
                                        </Card>
                                    </div>
                                )) : null}
                            </div>
                            
                        ))}
                        </>
                    : null}

                    {showReplyToPost ? 
                    <Box sx={{
                        display: 'flex',
                        alignItems: 'center',
                        flexDirection: 'column',
                        marginBottom: 1.3,
                        '& > :not(style)': { m: 1 },
                        }}>
                            {/* Reply to a post*/}
                        <Typography sx={{color: 'red'}}>{inputMessage}</Typography>
                        <TextField sx={{width: 300}} label="Title" onChange={(e) => setTitle(e.target.value)} />
                        <TextField sx={{width: 300}} label="Description" onChange={(e) => setDescription(e.target.value)} multiline rows={3} />
                        <Button variant="contained" onClick={() => replyToPost(posts.id)}>Reply</Button>
                    </Box> 
                    : null}
                </AccordionDetails>

                {editShowPost ? 
                <Box sx={{
                    display: 'flex',
                    alignItems: 'center',
                    flexDirection: 'column',
                    marginBottom: 1.3,
                    '& > :not(style)': { m: 1 },
                    }}>
                        {/* Edit a post out the pages */}
                    <Typography sx={{color: 'red'}}>{editInputMessage}</Typography>
                    <TextField sx={{width: 300}} value={editTitle} label="Title" onChange={(e) => setEditTitle(e.target.value)} />
                    <TextField sx={{width: 300}} value={editDescription} label="Description" onChange={(e) => setEditDescription(e.target.value)} multiline rows={3} />
                    <Button variant="contained" onClick={() => handleUpdate(posts.id)}>Update post</Button>
                </Box> 
                : null}
            </Accordion>

            {severity === "error" ? 
                <Snackbar open={open} autoHideDuration={5000} onClose={handleClose}>
                    <Alert severity="error">Error: Could not reply to new post!</Alert>
                </Snackbar> : 
                <Snackbar open={open} autoHideDuration={5000} onClose={handleClose}>
                    <Alert severity="success">Success: Replied to post!</Alert>
                </Snackbar>
            }

            {editSeverity === "error" ? 
                <Snackbar open={openEdit} autoHideDuration={5000} onClose={handleEditClose}>
                    <Alert severity="error">Error: Could not update the post!</Alert>
                </Snackbar> : 
                <Snackbar open={openEdit} autoHideDuration={5000} onClose={handleEditClose}>
                    <Alert severity="success">Success: Post has been updated!</Alert>
                </Snackbar>
            }
        </Card>
    )
}

export default SpecificGroupPost;
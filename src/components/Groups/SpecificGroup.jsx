import { Box, Button, Snackbar, TextField, Tooltip, Typography, MenuItem, Select, InputLabel, Card } from "@mui/material";
import { forwardRef, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { editGroup, getEventsBasedOnGroup, getGroupById, joinNewGroup } from "../../api/groupEndpoints";
import { getAllUsers, getSpecificUser } from "../../api/userEndpoints";
import LockIcon from '@mui/icons-material/Lock';
import LockOpenIcon from '@mui/icons-material/LockOpen';
import { createPost, getGroupPosts } from "../../api/postEndpoints";
import { useUser } from "../../context/UserContext";
import SpecificGroupPost from "./SpecificGroupPosts";
import AdminPanelSettingsIcon from '@mui/icons-material/AdminPanelSettings';
import MuiAlert from '@mui/material/Alert';
import SpecificUser from '../SpecificUser/SpecificUser'
import FormControl from '@mui/material/FormControl';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import FullCalendar from "@fullcalendar/react";
import dayGridPlugin from '@fullcalendar/daygrid'
import interactionPlugin from '@fullcalendar/interaction'
import moment from 'moment'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faLongArrowRight } from '@fortawesome/free-solid-svg-icons'
import withAuth from "../../hoc/withAuth";
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';
import SortIcon from '@mui/icons-material/Sort';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import ExpandLessIcon from '@mui/icons-material/ExpandLess';

const SpecificGroup = () => {
    // Different states
    const {user} = useUser()
    const { id } = useParams();
    const [group, setGroup] = useState([])
    const [groupMembers, setGroupMembers] = useState([])
    const [users, setUsers] = useState([])
    const [posts, setPosts] = useState([])
    const [creator, setCreator] = useState([])
    const [searchPost, setSearchPost] = useState("")
    const [searchUser, setSearchUser] = useState("")
    const [showNewPost, setShowNewPost] = useState(false);
    const [showAddNewMember, setShowAddNewMember] = useState(false)
    const [newUsers, setNewUsers] = useState([])
    const [pickedUser, setPickedUser] = useState("")
    const [searchForNewMember, setSearchForNewMember] = useState("")
    const [addMemberMessage, setAddMemberMessage] = useState("")
    const [newGroupMember, setNewGroupMember] = useState("")
    const [newest, setNewest] = useState(true)

    const [title, setTitle] = useState("")
    const [description, setDescription] = useState("")
    const [inputMessage, setInputMessage] = useState("")
    const [severity, setSeverity] = useState("")
    const [addSeverity, setAddSeverity] = useState("")
    const [open, setOpen] = useState(false)
    const [memberOpen, setMemberOpen] = useState(false);
    const [showEditGroup, setShowEditGroup] = useState(false)
    const [editGroupName, setEditGroupName] = useState("")
    const [editGroupDescription, setEditGroupDescription] = useState("")
    const [editGroupInputMessage, setEditGroupInputMessage] = useState("")
    const [confirm, setConfirm] = useState(false)
    const [severityEditgroup, setSeverityEditGroup] = useState("")
    const [editOpen, setEditOpen] = useState(false)
    const [status, setStatus] = useState(false)
    const [calenderOpen, setCalenderOpen] = useState(false)
    const [events, setEvents] = useState([])
    const [openEvent, setOpenEvent] = useState(false);
    const [showMoreInfo, setShowMoreInfo] = useState(false)

    useEffect(() => {
        // Abort controller for handling useEffect
        let abortController = new AbortController();

        // Async fetch to get all the necessary data
        const fetchData = async () => {
            const fetchedGroup = await getGroupById(id)
            const fetchedPosts = await getGroupPosts(id)
            const usersData = await getAllUsers();
            setNewUsers(usersData.data)
            setGroup(fetchedGroup.data)
            setGroupMembers(fetchedGroup.data.users)
            setEditGroupName(fetchedGroup.data.name)
            setEditGroupDescription(fetchedGroup.data.description)
            setStatus(fetchedGroup.data.isPrivate)

            setPosts(fetchedPosts.data)
            const fetchCreator = await getSpecificUser(fetchedGroup.data.creator)
            setCreator(fetchCreator)

            const fetchedEvents = await getEventsBasedOnGroup(id);
            setEvents(fetchedEvents.data)
        }
       
        fetchData();
        fetchUsers();

        return () => {
            abortController.abort();
        }
        
    }, [groupMembers.some(x => x)])

    // Seperate method for fetching all the users
    const fetchUsers = async () => {
        let usersList = []
        groupMembers.forEach(async (u) => {
            const response = await getSpecificUser(u)
            usersList.push(response)
            setUsers(usersList)
        })
    }

    // The calendar events
    const calendarEvents = () => {
        // Map the events and format them
        let mappedEvents = events.map((event) => (
            {
                title: event.name,
                start: moment(event.startTime).toISOString(), 
                end: moment(event.endTime).toISOString(),
            }
        ))

        return mappedEvents
    }

    // Method for creating a new post
    const createNewPost = async () => {
        // Checking so title and description is not empty
        if(title === "" || description === ""){
            setInputMessage("All the fields must contain a text")
        }
        else{
            setInputMessage("")

            // Object of the data that will be sent to api
            const currentDate = new Date().toISOString();
            const postData = {
                timeStamp: currentDate,
                title: title,
                description: description,
                senderId: user.id,
                userId: 0,
                groupId: id,
                eventId: 0,
                topicId: 0,
                replyId: 0
            }

            const response = await createPost(postData)

            // Depending on response, success or error
            if(response.status === 200){
                setSeverity("success")
                setOpen(true)
                const fetchedPosts = await getGroupPosts(id)
                setPosts(fetchedPosts.data)
                setShowNewPost(false)
            }

            if(response.status !== 200){
                setSeverity("error")
                setOpen(true)
            }
        }
    }

    // Edit group with confirmation modal
    const handleOpen = () => {
        if(editGroupName === "" || editGroupDescription === ""){
            setEditGroupInputMessage("All the fields must contain text")
        }
        else{
            setConfirm(true)
        }
    }

    // Method for edit the group data
    const editGroupData = async () => {
        handleConfirmClose();
        setEditGroupInputMessage("")

        // Format the data
        const postData = {
            id: id,
            name: editGroupName,
            creator: user.id,
            description: editGroupDescription,
            isPrivate: status
        }

        const response = await editGroup(postData)

        // Depending on response, success or error
        if(response.status === 204){
            setSeverityEditGroup("success")
            setEditOpen(true)
            const updatedGroup = await getGroupById(id)
            setGroup(updatedGroup.data)
            setShowEditGroup(false)
        }

        if(response.status !== 204){
            setSeverityEditGroup("error")
            setEditOpen(true)
        }
    }

    // Popup window formatting - Material UI
    const Alert = forwardRef(function Alert(props, ref) {
        return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
    });

    // Close the popup
    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
          return;
        }
    
        setOpen(false);
    };

    // Close the popup
    const handleAddClose = (event, reason) => {
        if(reason === 'clickaway'){
            return;
        }

        setMemberOpen(false)
    }

    // Close the popup
    const handleEditClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
          }
      
          setEditOpen(false);
    }

    // Handle dropdown for privacy status
    const handleChange = (event) => {
        setStatus(event.target.value);
    };

    // Helper method for setting user data 
    const setNewMember = (pickedUser) => {
        setNewGroupMember(pickedUser)
        setPickedUser(`${pickedUser.name}: ${pickedUser.workStatus}`)
        setSearchForNewMember("")
    }

    // Method for adding a new member
    const addNewMember = async () => {
        // Check if user has pickes a new member
        if(pickedUser === ""){
            setAddMemberMessage("You must pick a user!")
        }
        else{
            const response = await joinNewGroup(id, newGroupMember.id)

            // Depending on response, success or error
            if(response.status === 200){
                setAddSeverity("success")
                setMemberOpen(true)
                setShowAddNewMember(false)
                const fetchedGroup = await getGroupById(id)
                setGroupMembers(fetchedGroup.data.users)
            }

            if(response.status !== 200){
                setAddSeverity("error")
                setMemberOpen(true)
            }
        }
    }

    // Helpers for open and closing popups and confirmation
    const handleCalenderClose = () => {
        setCalenderOpen(false)
    }

    const showCalender = () => {
        setCalenderOpen(true)
    }

    const handleEventClickClose = () => {
        setOpenEvent(false);
    }

    const handleEventClickOpen = ({event}) => {
        setOpenEvent(true);
        events({event})   
    }

    const handleConfirmClose = () => {
        setConfirm(false);
    };

    return (
        <div>
            {/* Entire specific group*/}
            <Typography sx={{marginTop: 2}} variant="h4">{group.name}</Typography>
            <div style={{marginLeft: '3%'}}>
                {/* Show more info button about group*/}
            {showMoreInfo === false? 
                <Box sx={{display: 'flex', justifyContent: 'center'}}>  
                        {String(group.description).length > 30 ? 
                        <>
                            <Typography>{String(group.description).substring(0, 30)}</Typography>
                                <Tooltip title="Show more of the description" placement="right">
                                    <ExpandMoreIcon onClick={() => setShowMoreInfo(true)} fontSize="2" sx={{color: 'GrayText', marginLeft: 1, borderStyle: 'solid', backgroundColor: 'whitesmoke', borderColor: 'whitesmoke', borderRadius: 10}}/>
                            </Tooltip>
                        </>
                        : <Typography sx={{marginRight: "2.2%"}}>{group.description}</Typography>}
                </Box> 
            :
            <Box sx={{display: 'flex', justifyContent: 'center'}}>
                <Box sx={{width: '23%', borderBottom: 'solid 1px #E7E7E7'}}>  
                    <Typography>{group.description}</Typography>
                </Box>
                <Tooltip title="Show less of description" placement="right">
                    <ExpandLessIcon onClick={() => setShowMoreInfo(false)} fontSize="2" sx={{color: 'GrayText', marginLeft: 1, borderStyle: 'solid', backgroundColor: 'whitesmoke', borderColor: 'whitesmoke', borderRadius: 10}}/>
                </Tooltip>
            </Box>  
            }
            </div>
            
            {/* Tooltip for group creator */}
            <Tooltip title={`Group creator status: ${creator.workStatus}.   `} placement="bottom">
                <Box sx={{display: 'flex', justifyContent: 'center'}}>
                    <Typography sx={{color: 'lightslategrey', marginRight: 0.5}}><AdminPanelSettingsIcon sx={{fontSize: 17, marginRight: 0.3}}/>Group creator</Typography><Typography>• {creator.name} </Typography>
                </Box>
            </Tooltip>

            {/* Styling and checking accessability if group creator or not*/}
            {group.isPrivate === true 
            ? 
                <div>
                    <Box sx={{display: 'flex', justifyContent: 'center'}}>
                        <Typography sx={{color: 'lightslategrey', marginRight: 0.5}}><LockIcon sx={{fontSize: 15, marginRight: 0.3}}/>Private group</Typography><Typography sx={{marginRight: 0.5}}>•</Typography><Typography>{groupMembers.length} members</Typography>
                    </Box>
                    <Button variant="contained" sx={{marginTop: 1, marginBottom: 2}} onClick={() => setShowNewPost(!showNewPost)}>{!showNewPost ? "create new post" : "hide"}</Button>
                    {group.creator === user.id ? 
                    <>
                        <Button variant="contained" sx={{marginTop: 1, marginBottom: 2, marginLeft: 1}} onClick={() => setShowAddNewMember(!showAddNewMember)}>{!showAddNewMember ? "Add member" : "hide"}</Button>
                        <Button sx={{marginTop: 1, marginBottom: 2, marginLeft: 1}} variant="contained" onClick={() => setShowEditGroup(!showEditGroup)}>{!showEditGroup ? "Edit group" : "hide"}</Button>
                    </>
                    : null}
                    
                    <Tooltip title="Show group events" placement="right">
                        <Button variant="outlined" sx={{marginTop: 1, marginBottom: 2, marginLeft: 1}}>
                            <CalendarMonthIcon  onClick={() => showCalender()}/>
                        </Button>
                    </Tooltip>
                </div>
                
            :
                <div>
                    <Box sx={{display: 'flex', justifyContent: 'center',  marginBottom: 2}}>
                        <Typography sx={{color: 'lightslategrey', marginRight: 0.5}}><LockOpenIcon sx={{fontSize: 15, marginRight: 0.3}}/>Public group</Typography><Typography sx={{marginRight: 0.5}}>•</Typography><Typography>{groupMembers.length} members</Typography>
                    </Box>
                    <Button variant="contained" sx={{marginTop: 1, marginBottom: 2}}  onClick={() => setShowNewPost(!showNewPost)}>{!showNewPost ? "create new post" : "hide"}</Button>

                    {group.creator === user.id ? 
                    <>
                        <Button variant="contained" sx={{marginTop: 1, marginBottom: 2, marginLeft: 1}} onClick={() => setShowAddNewMember(!showAddNewMember)}>{!showAddNewMember ? "Add member" : "hide"}</Button>
                        <Button sx={{marginTop: 1, marginBottom: 2, marginLeft: 1}} variant="contained" onClick={() => setShowEditGroup(!showEditGroup)}>{!showEditGroup ? "Edit group" : "hide"}</Button>
                    </>
                    : null}

                    <Tooltip title="Show group events" placement="right">
                        <Button variant="outlined" sx={{marginTop: 1, marginBottom: 2, marginLeft: 1}}>
                            <CalendarMonthIcon  onClick={() => showCalender()}/>
                        </Button>
                    </Tooltip>
                </div> 
            }

            {/* Create new post */}
            {showNewPost ? 
                <Box sx={{
                    display: 'flex',
                    alignItems: 'center',
                    flexDirection: 'column',
                    marginBottom: 1.3,
                    '& > :not(style)': { m: 1 },
                    }}>
                    <Typography sx={{color: 'red'}}>{inputMessage}</Typography>
                    <TextField sx={{width: 300}} label="Title" onChange={(e) => setTitle(e.target.value)} />
                    <TextField sx={{width: 300}} label="Description" onChange={(e) => setDescription(e.target.value)} multiline rows={5} />
                    <Button variant="contained" onClick={() => createNewPost()}>Create</Button>
                </Box>
            : null}

            {/* Add new member */}
            {showAddNewMember ? 
                <Box sx={{
                    display: 'flex',
                    alignItems: 'center',
                    flexDirection: 'column',
                    marginBottom: 1.3,
                    '& > :not(style)': { m: 1 },
                    }}
                    >
                        <Typography sx={{color: 'red'}}>{addMemberMessage}</Typography>
                        <TextField label="Receiver (name)" sx={{width: 400}} type="text" autoComplete="off" placeholder="Search for user" onChange={(e) => setSearchForNewMember(e.target.value)} />
                        {newUsers.filter((u) => {
                            if(searchForNewMember === ""){
                                return u
                            }
                            else if(u.name.toLowerCase().includes(searchForNewMember.toLowerCase())){
                                return u
                            }
                        }).map((u, i) => (
                            <>
                            {searchForNewMember === "" 
                                ? null 
                                : <>
                                    <Button key={i} sx={{marginTop: 0.5}} onClick={() => setNewMember(u)}>{u.name}: {u.workStatus}</Button>
                                </>
                                }
                            </>
                                
                        ))}
                    
                    <TextField value={pickedUser} disabled={true} sx={{width: 400}}/>
                    <Button variant="contained" onClick={() => addNewMember()}>Add new member</Button>
                </Box>
            : null}

            {/* Edit group */}
            {showEditGroup ? 
                <Box sx={{
                    display: 'flex',
                    alignItems: 'center',
                    flexDirection: 'column',
                    marginBottom: 1.3,
                    '& > :not(style)': { m: 1 },
                    }}>
                        <Typography sx={{color: 'red'}}>{editGroupInputMessage}</Typography>
                        <TextField label="Group name" sx={{width: 400}} type="text" autoComplete="off" value={editGroupName}  onChange={(e) => setEditGroupName(e.target.value)} />
                        <TextField  multiline rows={5} label="Group description" sx={{width: 400}} type="text" autoComplete="off" value={editGroupDescription}  onChange={(e) => setEditGroupDescription(e.target.value)} />
                        <Box >
                            <FormControl sx={{ minWidth: 400 }}>
                                <InputLabel >Set private status</InputLabel>
                                <Select
                                    label="Set private status"
                                    id="select"
                                    value={status}
                                    onChange={handleChange}
                                >
                                <MenuItem value={true}>Private</MenuItem>
                                <MenuItem value={false}>Not private</MenuItem>
                            </Select>
                        </FormControl>
                        </Box>
                        <Button variant="contained" onClick={handleOpen}>Edit group</Button>
                </Box>
            : null}
            <div style={{display: 'flex', justifyContent: 'center', borderTop: "1px solid #E7E7E7"}}>
                    <Box sx={{marginBottom: 4, marginRight: 4, width: 500, borderRight: "1px solid #E7E7E7"}}> 
                        <Typography sx={{marginTop: 2}} variant="h5">Members</Typography>
                        <TextField label="Search for group member" sx={{marginBottom: 2, marginTop: 1, width: '95%'}} type="text" autoComplete="off" onChange={(e) => setSearchUser(e.target.value)}/>
                        
                        {users.filter((u, i) => {
                        if(searchUser === ""){
                            return u
                        }
                        if(u.name.toLowerCase().includes(searchUser.toLowerCase())){
                            return u
                        }
                    }).map((u, i) => (
                        <div key={i}>
                            <SpecificUser users={u} key={i}/>
                        </div>
                    ))}

                    </Box>
                    <Box sx={{marginBottom: 9, marginLeft: 6, borderLeft: "1px solid #E7E7E7", width: 600}}>
                        {/* Newest/oldest posts*/}
                        <Typography sx={{marginTop: 2, marginBottom: 1}} variant="h5">Posts</Typography>
                        <Box sx={{display: 'flex', justifyContent: 'space-evenly', marginTop: 1}}>                                                            
                            <TextField label="Search for post" sx={{marginBottom: 2, width: '75%'}} type="text" autoComplete="off" onChange={(e) => setSearchPost(e.target.value)}/>
                            <Button variant="outlined" sx={{marginBottom: 3, marginTop: 1, fontSize: 13}}onClick={() => setNewest(!newest)}>{newest ? <><SortIcon/>Recent</> : <><SortIcon/>Oldest</>}</Button>
                        </Box>
                        {newest ? <>
                           
                                {posts.filter((post) => {
                                if(searchPost === ""){
                                    return post
                                }
                                else if(post.title.toLowerCase().includes(searchPost.toLowerCase())){
                                    return post
                                }
                                }).map((post, i) => (
                                    <div key={i}>
                                        {!post.replyId ? <SpecificGroupPost posts={post} key={i}/> : null}
                                    </div>
                                ))}
                        </> : 
                        <>
                            {posts.filter((post) => {
                                if(searchPost === ""){
                                    return post
                                }
                                else if(post.title.toLowerCase().includes(searchPost.toLowerCase())){
                                    return post
                                }
                            }).map((post, i) => (
                                <div key={i}>
                                    {!post.replyId ? <SpecificGroupPost posts={post} key={i}/> : null}
                                </div>
                            )).reverse()}
                        </>}
                        
                    </Box>
            </div>
            
            {/* Alerts */}
            {severity === "error" ? 
                <Snackbar open={open} autoHideDuration={5000} onClose={handleClose}>
                    <Alert severity="error">Error: Could not create a new post!</Alert>
                </Snackbar> : 
                <Snackbar open={open} autoHideDuration={5000} onClose={handleClose}>
                    <Alert severity="success">Success: New post created!</Alert>
                </Snackbar>
            }

            {addSeverity === "error" ? 
                <Snackbar open={memberOpen} autoHideDuration={5000} onClose={handleAddClose}>
                    <Alert severity="error">Error: Could not add a new member!</Alert>
                </Snackbar> : 
                <Snackbar open={memberOpen} autoHideDuration={5000} onClose={handleAddClose}>
                    <Alert severity="success">Success: New member added! Go back then into group to see the new member</Alert>
                </Snackbar>
            }

            {severityEditgroup === "error" ? 
                <Snackbar open={editOpen} autoHideDuration={5000} onClose={handleEditClose}>
                    <Alert severity="error">Error: Could not update group!</Alert>
                </Snackbar> : 
                <Snackbar open={editOpen} autoHideDuration={5000} onClose={handleEditClose}>
                    <Alert severity="success">Success: Group updated!</Alert>
                </Snackbar>
            }

            {/* Dialog conformation */}
            <Dialog
                open={confirm}
                onClose={handleOpen}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
                >
                <DialogTitle id="alert-dialog-title">
                    {"Confirmation"}
                </DialogTitle>
                <DialogContent>
                <DialogContentText id="alert-dialog-description">
                    Do you want to update this group?
                </DialogContentText>
                </DialogContent>
                <DialogActions>
                <Button onClick={handleConfirmClose}>No</Button>
                <Button onClick={() => editGroupData()}>
                    Yes
                </Button>
                </DialogActions>
            </Dialog>


            <Dialog
                open={calenderOpen}
                keepMounted
                onClose={handleCalenderClose}
                aria-describedby="alert-dialog-slide-description"
                fullWidth
                maxWidth="lg"
                style={{backgroundColor: "#1976d2"}}
                >
                <DialogTitle>{"Group events"}</DialogTitle>
                    <DialogContent>
                        <FullCalendar
                        plugins={[ dayGridPlugin, interactionPlugin ]}
                        initialView="dayGridMonth"
                        events={calendarEvents()}
                        eventClick={handleEventClickOpen}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleCalenderClose}>Close</Button>
                    </DialogActions>
                </Dialog>

                <Dialog
                open={openEvent}
                keepMounted
                onClose={handleEventClickClose}
                aria-describedby="alert-dialog-slide-description"
                maxWidth="md"
                >
                    <DialogTitle>
                        {events.title}
                    </DialogTitle>
                    <DialogContent >
                        <Card elevation={3} 
                        style={{
                            display:"flex", 
                            flexDirection:"row", 
                            justifyContent:"space-between",
                            padding: "20",
                            minWidth: "50vw"}}>
                            <p style={{marginLeft: "2vw"}}> <span style={{fontWeight: "bold"}}>Started at</span>  {moment(events.start).format('yyyy/MM/DD - H:mma')}</p>
                            <p><FontAwesomeIcon icon={faLongArrowRight} /></p> 
                            <p style={{marginRight: "2vw"}}> <span style={{fontWeight: "bold"}}>Ends at</span>  {moment(events.end).format('yyyy/MM/DD - H:mma ') }</p>
                        </Card>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleEventClickClose}>Close</Button>
                    </DialogActions>
            </Dialog>

        </div>
    )
}

export default withAuth(SpecificGroup);
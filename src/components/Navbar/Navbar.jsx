import {useEffect, useState} from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Menu from '@mui/material/Menu';
import MenuIcon from '@mui/icons-material/Menu';
import Container from '@mui/material/Container';
import Button from '@mui/material/Button';
import Tooltip from '@mui/material/Tooltip';
import MenuItem from '@mui/material/MenuItem';
import AccountBoxRoundedIcon from '@mui/icons-material/AccountBoxRounded';
import keycloak from '../../keycloak';
import { createNewUser, getUserBasedOnEmail } from '../../api/userEndpoints';
import { useUser } from '../../context/UserContext';
import { storageDelete, storageSave } from '../../utils/storage';
import { STORAGE_KEY_USER } from '../../const/storagekeys';
import BookIcon from '@mui/icons-material/Book';

const Navbar = () => {
  const [anchorElNav, setAnchorElNav] = useState(null);
  const [anchorElUser, setAnchorElUser] = useState(null);
  const [pages, setPages] = useState([])
  const [settings, setSettings] = useState([])
  const navigate = useNavigate()
  const location = useLocation();
  const {user, setUser} = useUser();

  useEffect(() => {
    let ignore = false;
    // If kc is auth
    if (keycloak.authenticated) {
        // Fetch the data
        const fetchData = async () => {
          // Get the users based on email
          const userResponse = await getUserBasedOnEmail(keycloak.tokenParsed.email)
          // If not exists in database
          if(userResponse.status === 204){
            // Create
              const postData = {
                  name: keycloak.tokenParsed.name,
                  email: keycloak.tokenParsed.email,
                  picture: "",
                  workStatus: "",
                  bio: "",
                  funFact: ""
              }
              // ignore ignores the first useEffect render, otherwise a user will be created twice
              if(!ignore) {
                await createNewUser(postData);
              }
            
          }

          // Then login
          await storeLogin();
        }

        // If the user is null, fetch the data
        if(user === null){
          fetchData();
        }

        // Redirect to timeline if user is not null and path is /
        if(location.pathname === "/" && user !== null){
          navigate("/timeline")
        }
        // Set the menu options
        setPages(['Timeline', 'Groups', 'Events', 'Topics', 'Profiles'])
        setSettings(['Profile', 'Logout']);
    }

    // If kc not auth
    if(!keycloak.authenticated){
      // Clear everything and "logout"
      storageDelete(STORAGE_KEY_USER)
      setUser(null);
      navigate("/")
    }

    return () => {
      ignore = true;
    }
  }, [])

  // Open the menus for navbar
  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };
  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };

  // Handle logout method
  const handleLogout = () => {
    storageDelete(STORAGE_KEY_USER);
    setUser(null);
    keycloak.logout();
    navigate("/")
  }

  // Method for login 
  const storeLogin = async () => {
    // Fetch the kc user profile
    const response = await keycloak.loadUserProfile();
    // Get the user
    const profile = await getUserBasedOnEmail(response.email)
    // Set the values
    setUser(profile.data)
    storageSave(STORAGE_KEY_USER, profile.data);
    // Navigate to timeline if keycloak authed
    if(keycloak.authenticated){
      navigate("/timeline")
    }
    else{
      navigate("/")
    }
  }

  // Method for handling the menu options
  const handlePageChange = async (menu) => {
    if(keycloak.authenticated){
      if(menu === "MainMenuOption") navigate("/timeline")
      if(menu === "Timeline") navigate("/timeline")
      if(menu === "Groups") navigate("/groups")
      if(menu === "Events") navigate("/events")
      if(menu === "Topics") navigate("/topics")
      if(menu === "Profiles") navigate("/profiles")
      if(menu === "Profile") navigate("/userprofile")
      if(menu === "Logout") handleLogout();
      if(menu === "Login") storeLogin();
    }else{
      if(menu === "MainMenuOption") navigate("/")
    }
  }
  return (
    <AppBar position="static">
      {/* The entire app bar*/}
      <Container maxWidth="xl">
        <Toolbar disableGutters>
          <BookIcon sx={{ display: { xs: 'none', md: 'flex' }, mr: 1 }} />
          <Typography
            variant="h6"       
            noWrap
            onClick={() => handlePageChange("MainMenuOption")}
            sx={{
              mr: 2,
              display: { xs: 'none', md: 'flex' },
              fontFamily: 'monospace',
              fontWeight: 700,
              letterSpacing: '.3rem',
              color: 'inherit',
              textDecoration: 'none',
              ":hover":{
                cursor: 'pointer'
              }
            }}
          >
            AlumniNetwork
          </Typography>
          {!keycloak.authenticated 
          ? null
          :
            <Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
              <IconButton
                size="large"
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                onClick={handleOpenNavMenu}
                color="inherit"
              >
              <MenuIcon />
              </IconButton>
              <Menu
                id="menu-appbar"
                anchorEl={anchorElNav}
                anchorOrigin={{
                  vertical: 'bottom',
                  horizontal: 'left',
                }}
                keepMounted
                transformOrigin={{
                  vertical: 'top',
                  horizontal: 'left',
                }}
                open={Boolean(anchorElNav)}
                onClose={handleCloseNavMenu}
                sx={{
                  display: { xs: 'block', md: 'none' },
                }}
                >
                  {/* Maps out the pages */}
                {pages.map((page, i) => (
                  <MenuItem key={i} onClick={() => {handlePageChange(page)}}>
                      <Typography key={i} textAlign="center">{page}</Typography>
                  </MenuItem>
                ))}
              </Menu>
            </Box>
          }
          
          <BookIcon sx={{ display: { xs: 'flex', md: 'none' }, mr: 1 }} />
          <Typography
            variant="h5"
            noWrap
            component="a"
            href="/"
            sx={{
              mr: 2,
              display: { xs: 'flex', md: 'none' },
              flexGrow: 1,
              fontFamily: 'monospace',
              fontWeight: 700,
              letterSpacing: '.3rem',
              color: 'inherit',
              textDecoration: 'none',
            }}
          >
            AlumniNetwork
          </Typography>
          <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}>
            {/* Maps out the pages */}
            {pages.map((page) => (
              <Button
                key={page}
                onClick={() => {handlePageChange(page)}}
                sx={{ my: 2, color: 'white', display: 'block' }}
              >
                {page}
              </Button>
            ))}
          </Box>
            {!keycloak.authenticated 
            ? null 
            : <Box sx={{ flexGrow: 0 }}>
                <Tooltip title="Options">
                  <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                    <AccountBoxRoundedIcon  sx={{color: 'white', fontSize: 33}}/>
                  </IconButton>
                </Tooltip>
              <Menu
                sx={{ mt: '45px' }}
                id="menu-appbar"
                anchorEl={anchorElUser}
                anchorOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                keepMounted
                transformOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                open={Boolean(anchorElUser)}
                onClose={handleCloseUserMenu}
              >
                {/* Maps out the settings */}
              {settings.map((setting) => (
                <MenuItem key={setting} onClick={() => {handlePageChange(setting)}}>
                  <Typography textAlign="center">{setting}</Typography>
                </MenuItem>
              ))}
            </Menu>
            </Box>
            }
          
        </Toolbar>
      </Container>
    </AppBar>
  );
};
export default Navbar;
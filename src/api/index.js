import axios from "axios"
import keycloak from "../keycloak"

export const createHeaders = {
    headers: {
      'Content-Type': 'application/json',
    }
}

const setAuthorizationHeader = (headers, keycloak) => {
  const { token } = keycloak;
  return {
    ...headers,
    "Content-Type": "application/json",
    Authorization: `Bearer ${token}`,
  };
};

axios.interceptors.request.use(async (config) => {
  if(!keycloak.authenticated){
    return config
  }

  if(!keycloak.isTokenExpired()){
    return{
      ...config,
      headers: setAuthorizationHeader(config.headers, keycloak)
    };
  }

  const HOUR_IN_SECONDS = 3600;

  try {
    await keycloak.updateToken(HOUR_IN_SECONDS);
  } catch (error) {
    console.log("Could not refresh Keycloak Token:  Axios Interceptor");
  }

  return {
    ...config,
    headers: setAuthorizationHeader(config.headers, keycloak)
  };
})

export default axios
import axios from '.'

const API_TOPIC_URL = "https://alumninetworkbackendapi20220921145336.azurewebsites.net/topic"

// Gets all the topics
export const getAllTopics = async () => {
    return await axios.get(`${API_TOPIC_URL}`)
}

// Assigns a user to a topic
export const assignUserToTopicByUserId = async (topicId, userId) => {
   const response = await axios.post(`${API_TOPIC_URL}/${topicId}/join?userId=${userId}`)
   return response;
}

// Creates a topic and assigns the creator as subscriber
export const createTopicAndAssignCreatorAsSubscriber = async (userId, params) => {
   const response = await axios.post(`${API_TOPIC_URL}/${userId}`, JSON.stringify(params));
   return response;
}

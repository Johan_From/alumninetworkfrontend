import axios from ".";

const API_URL = "https://alumninetworkbackendapi20220921145336.azurewebsites.net/group"
const API_EVENT = "https://alumninetworkbackendapi20220921145336.azurewebsites.net/event"

// Fetches all the groups
export const getAllGroupsWithUser = async (id) => {
    const response = await axios.get(`${API_URL}/searchString/limit`)
    return response
}

// Joins a new group
export const joinNewGroup = async (groupId, userId) => {
    try {
        const response = await axios.put(`${API_URL}/join/${groupId}?userId=${userId}`)   
        return response;
    } catch (error) {
        return error;
    }
}

//Gets a group by id
export const getGroupById = async (id) => {
    if(id === 0){
        return;
    }
    else{
        const response = await axios.get(`${API_URL}/${id}`)
        return response;
    }
}

// Creates a new group
export const createNewGroup = async (id, params) => {
    try {
        const response = await axios.post(`${API_URL}?userId=${id}`, JSON.stringify(params))
        return response
    } catch (error) {
        return error
    }
}

// Edits a group
export const editGroup = async (params) => {
    try {
        const response = await axios.put(`${API_URL}/${params.id}`, JSON.stringify(params))
        return response
    } catch (error) {
        return error
    }
}

// Get event based on a group
export const getEventsBasedOnGroup = async (id) => {
    const response = await axios.get(`${API_EVENT}/group/${id}`)
    return response;
}
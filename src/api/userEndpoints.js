import axios from '.'

const URL = "https://alumninetworkbackendapi20220921145336.azurewebsites.net/user"

//Fetches all users
export const getAllUsers = async () => {
    const response =  await axios.get(URL+"s");
    return response
}

//Fetches specific user by id
export const getSpecificUser =  (id) => {
    return axios.get(`${URL}/${id}`).then(response => {
       return response.data
       
    });
}

//Fetches user based on email
export const getUserBasedOnEmail = async (email) => {
    return await axios.get(`${URL}/email/${email}`)
}

//Creates a user with error handling
export const createNewUser = async (params) => {
    try {
        const res = await axios.post(URL, JSON.stringify(params));
        return res
    } catch (error) {
        return error
    }   
}

//Patch user with error handlings
export const patchUser = async (params) => {
    try {
        const response = await axios.patch(`${URL}/userId?userId=${params.id}`, JSON.stringify(params));
        return response   
    } catch (error) {
        return error
    }
}

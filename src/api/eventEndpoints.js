import axios from '.'

const API_URL = "https://alumninetworkbackendapi20220921145336.azurewebsites.net/event"

// Fetches events by user id
export const getEventsByUserId = async (id) => {
    let response =  await axios.get(`${API_URL}/user/${id}`);
    return response;
} 

// Creates a new event
export const createNewEvent = async (params) => {
    const response = await axios.post(`${API_URL}`, JSON.stringify(params)) //.catch(error => console.log(error));
    return response;
}

// Fetches events the user have created
export const getEventByEventId = async (id) => {
    let response = await axios.get(`${API_URL}/${id}`);
    return response;
}

// Fetches events from groups and topics which the user are subscribed to
export const getEventsBySubscription = async (id) => {
    const response = await axios.get(`${API_URL}/subscribed/${id}`)
    return response;
}

// Invites an event to the selected group
export const inviteGroupToEvent = async (groupId, eventId) => {
    const response = await axios.post(`${API_URL}/${eventId}/invite/group/${groupId}`).catch( error => console.log(error));
    return response;
}
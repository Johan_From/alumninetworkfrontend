import axios from '.'

const API_URL = "https://alumninetworkbackendapi20220921145336.azurewebsites.net/post"

// Gets all the post from a user
export const getAllPostsFromUser = async (id) => {
    try {
        const response = await axios.get(`${API_URL}?userId=${id}`);
        return response
    } catch (error) {
        return 404
    }
}

// Creates a new post
export const createPost = async (params) => {
    try {
        const response = await axios.post(API_URL+`?userId=${params.senderId}`, JSON.stringify(params))
        return response
    } catch (error) {
        return error.message
    }
}

// Get all post replies by id
export const getPostRepliesById = async (id) => {
   return await axios.get(`${API_URL}s/replies/${id}`)
}

// Gets the group posts
export const getGroupPosts = async (groupId) => {
    return await axios.get(`${API_URL}/group/${groupId}`)
}

// Get direct message posts
export const getDirectPostToUser = async (userId) => {
    return await axios.get(`${API_URL}/direct/user/${userId}`)
}

// Gets the topic posts based on topic
export const getTopicPostsBasedOnTopicId = async (topicId) => {
    return await axios.get(`${API_URL}/topic/${topicId}`)
}

// Updates a post
export const updatePost = async (params) => {
    try {
        const response = await axios.put(`${API_URL}/${params.id}`, JSON.stringify(params))
        return response
    } catch (error) {
        return error
    }
}
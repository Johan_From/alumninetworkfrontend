# AlumniNetworkFrontend
An online single web application that makes it possible for alumni to communicate with each other. You can send messages, join/vists groups, create posts topics and events.  

## Table of Contents
1. [General Info](#general-info)
2. [Technologies Used](#technologies-used)
3. [Setup](#setup)
4. [Collaborators](#collaborators)

## General Information
* The app is written in JavaScript and utalizes the react framework
* Deeper understanding of working with API:es
* Understand the react lyfecycle and hooks


## Technologies Used
* JavaScript 
* React Framework
* Material UI
* Axios

## Setup
To get the console application started started follow the steps below:

1. This application was written with Visual Studio 2022 
2. Clone this repository ```https://gitlab.com/Johan_From/alumninetworkfrontend.git```
3. Navigate to the folder ```alumninetworkfrontend``` and follow these steps in the terminal:

```
> npm i
> npm start
running on host: http://localhost:3000
```

## Collaborators
* [Johan From](https://gitlab.com/Johan_From)
* [Armin Ljajic](https://gitlab.com/ArminExperis)
* [Anders Hansen-Haug](https://gitlab.com/AndersHHexperis)

